<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190308121230 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE application_form (id INT AUTO_INCREMENT NOT NULL, call_id INT NOT NULL, project_id INT NOT NULL, current_employer VARCHAR(255) DEFAULT NULL, position_title VARCHAR(255) DEFAULT NULL, position_since DATETIME DEFAULT NULL, previous_positions LONGTEXT DEFAULT NULL, ias_knowledge LONGTEXT DEFAULT NULL, previous_ias_member LONGTEXT DEFAULT NULL, major_puplications LONGTEXT DEFAULT NULL, motivations LONGTEXT DEFAULT NULL, ongoing_application LONGTEXT DEFAULT NULL, benefits LONGTEXT DEFAULT NULL, biography LONGTEXT DEFAULT NULL, sabbatical VARCHAR(255) DEFAULT NULL, financial_support LONGTEXT DEFAULT NULL, usual_salary VARCHAR(255) DEFAULT NULL, accompanying LONGTEXT DEFAULT NULL, reside_in_nantes TINYINT(1) DEFAULT NULL, created_at DATETIME DEFAULT NULL, last_update_at DATETIME DEFAULT NULL, submitted_at DATETIME DEFAULT NULL, final_decision VARCHAR(255) DEFAULT NULL, choice1number_months INT DEFAULT NULL, choice1start_month INT DEFAULT NULL, choice2number_months INT DEFAULT NULL, choice2start_month INT DEFAULT NULL, file_curriculum_vitae VARCHAR(255) DEFAULT NULL, file_application_form VARCHAR(255) DEFAULT NULL, INDEX IDX_A56495C750A89B2C (call_id), INDEX IDX_A56495C7166D1F9C (project_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ask_contact (id INT AUTO_INCREMENT NOT NULL, lastname VARCHAR(255) NOT NULL, firstname VARCHAR(255) DEFAULT NULL, email VARCHAR(255) NOT NULL, information LONGTEXT NOT NULL, created_at DATETIME DEFAULT NULL, processed_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ask_login (id INT AUTO_INCREMENT NOT NULL, lastname VARCHAR(255) NOT NULL, firstname VARCHAR(255) DEFAULT NULL, email VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, created_at DATETIME DEFAULT NULL, project VARCHAR(1024) NOT NULL, grpd_consent_at DATETIME DEFAULT NULL, status INT DEFAULT NULL, processed_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE call_app (id INT AUTO_INCREMENT NOT NULL, scientific_council_id INT NOT NULL, label_fr VARCHAR(255) NOT NULL, description_fr LONGTEXT DEFAULT NULL, label_en VARCHAR(255) NOT NULL, description_en LONGTEXT DEFAULT NULL, opening_date DATETIME NOT NULL, closing_date DATETIME NOT NULL, image VARCHAR(255) DEFAULT NULL, INDEX IDX_9032DC24142CCA8E (scientific_council_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE country (id INT AUTO_INCREMENT NOT NULL, region_id INT DEFAULT NULL, alpha_code2 VARCHAR(2) NOT NULL, alpha_code3 VARCHAR(3) DEFAULT NULL, label_fr VARCHAR(255) DEFAULT NULL, label_en VARCHAR(255) DEFAULT NULL, INDEX IDX_5373C96698260155 (region_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE evaluation (id INT AUTO_INCREMENT NOT NULL, application_form_id INT DEFAULT NULL, expert_id INT NOT NULL, grade VARCHAR(2) DEFAULT NULL, comment LONGTEXT DEFAULT NULL, requested_at DATETIME DEFAULT NULL, is_accepted TINYINT(1) DEFAULT NULL, reminded_at DATETIME DEFAULT NULL, due_at DATETIME DEFAULT NULL, thanks_sent_at DATETIME DEFAULT NULL, internal_notes LONGTEXT DEFAULT NULL, type_evaluation INT DEFAULT NULL, INDEX IDX_1323A575B4489E4E (application_form_id), INDEX IDX_1323A575C5568CE4 (expert_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE field (id INT AUTO_INCREMENT NOT NULL, label VARCHAR(255) NOT NULL, description LONGTEXT DEFAULT NULL, label_en VARCHAR(255) NOT NULL, description_en LONGTEXT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE language (id INT AUTO_INCREMENT NOT NULL, iso639_3 VARCHAR(3) DEFAULT NULL, iso639_2 VARCHAR(3) DEFAULT NULL, label_en VARCHAR(255) NOT NULL, label_fr VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE nationality (id INT AUTO_INCREMENT NOT NULL, country_id INT DEFAULT NULL, label_fr VARCHAR(255) DEFAULT NULL, label_en VARCHAR(255) DEFAULT NULL, INDEX IDX_8AC58B70F92F3E70 (country_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE person (id INT AUTO_INCREMENT NOT NULL, lastname VARCHAR(255) NOT NULL, firstname VARCHAR(255) DEFAULT NULL, usual_lastname VARCHAR(255) DEFAULT NULL, usual_firstname VARCHAR(255) DEFAULT NULL, gender INT DEFAULT NULL, date_of_birth DATETIME DEFAULT NULL, place_of_birth VARCHAR(255) DEFAULT NULL, email VARCHAR(255) NOT NULL, password VARCHAR(255) DEFAULT NULL, created_at DATETIME DEFAULT NULL, verified_email_at DATETIME DEFAULT NULL, grpd_consent_at DATETIME DEFAULT NULL, web_page VARCHAR(255) DEFAULT NULL, ph_dyear VARCHAR(255) DEFAULT NULL, ph_duniversity VARCHAR(255) DEFAULT NULL, title VARCHAR(255) DEFAULT NULL, reset_password_token VARCHAR(255) DEFAULT NULL, token_expire_at DATETIME DEFAULT NULL, phone_number VARCHAR(255) DEFAULT NULL, french_level VARCHAR(255) DEFAULT NULL, english_level VARCHAR(255) DEFAULT NULL, expert TINYINT(1) DEFAULT NULL, expert_start DATE DEFAULT NULL, expert_end DATE DEFAULT NULL, expert_reasonend VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE person_nationality (person_id INT NOT NULL, nationality_id INT NOT NULL, INDEX IDX_E72ED4F8217BBB47 (person_id), INDEX IDX_E72ED4F81C9DA55 (nationality_id), PRIMARY KEY(person_id, nationality_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE person_field (person_id INT NOT NULL, field_id INT NOT NULL, INDEX IDX_F7C35A9C217BBB47 (person_id), INDEX IDX_F7C35A9C443707B0 (field_id), PRIMARY KEY(person_id, field_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE person_scientific_council (person_id INT NOT NULL, scientific_council_id INT NOT NULL, INDEX IDX_C6760C0F217BBB47 (person_id), INDEX IDX_C6760C0F142CCA8E (scientific_council_id), PRIMARY KEY(person_id, scientific_council_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE postal_address (id INT AUTO_INCREMENT NOT NULL, person_id INT NOT NULL, country_id INT NOT NULL, label LONGTEXT NOT NULL, updated_at DATETIME NOT NULL, UNIQUE INDEX UNIQ_972EFBF7217BBB47 (person_id), INDEX IDX_972EFBF7F92F3E70 (country_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE project (id INT AUTO_INCREMENT NOT NULL, main_field_id INT DEFAULT NULL, person_id INT NOT NULL, title VARCHAR(255) NOT NULL, summary LONGTEXT NOT NULL, main_subjects LONGTEXT DEFAULT NULL, previous_publications_enlightning LONGTEXT DEFAULT NULL, specialty VARCHAR(255) DEFAULT NULL, conciliate_previous_works LONGTEXT DEFAULT NULL, file_project VARCHAR(255) DEFAULT NULL, file_publication1 VARCHAR(255) DEFAULT NULL, file_publication2 VARCHAR(255) DEFAULT NULL, INDEX IDX_2FB3D0EE9D1516C5 (main_field_id), INDEX IDX_2FB3D0EE217BBB47 (person_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE region (id INT AUTO_INCREMENT NOT NULL, label_fr VARCHAR(255) NOT NULL, label_en VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE role (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE role_person (role_id INT NOT NULL, person_id INT NOT NULL, INDEX IDX_9FFA30C7D60322AC (role_id), INDEX IDX_9FFA30C7217BBB47 (person_id), PRIMARY KEY(role_id, person_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE scientific_council (id INT AUTO_INCREMENT NOT NULL, label VARCHAR(255) NOT NULL, description LONGTEXT DEFAULT NULL, date DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE application_form ADD CONSTRAINT FK_A56495C750A89B2C FOREIGN KEY (call_id) REFERENCES call_app (id)');
        $this->addSql('ALTER TABLE application_form ADD CONSTRAINT FK_A56495C7166D1F9C FOREIGN KEY (project_id) REFERENCES project (id)');
        $this->addSql('ALTER TABLE call_app ADD CONSTRAINT FK_9032DC24142CCA8E FOREIGN KEY (scientific_council_id) REFERENCES scientific_council (id)');
        $this->addSql('ALTER TABLE country ADD CONSTRAINT FK_5373C96698260155 FOREIGN KEY (region_id) REFERENCES region (id)');
        $this->addSql('ALTER TABLE evaluation ADD CONSTRAINT FK_1323A575B4489E4E FOREIGN KEY (application_form_id) REFERENCES application_form (id)');
        $this->addSql('ALTER TABLE evaluation ADD CONSTRAINT FK_1323A575C5568CE4 FOREIGN KEY (expert_id) REFERENCES person (id)');
        $this->addSql('ALTER TABLE nationality ADD CONSTRAINT FK_8AC58B70F92F3E70 FOREIGN KEY (country_id) REFERENCES country (id)');
        $this->addSql('ALTER TABLE person_nationality ADD CONSTRAINT FK_E72ED4F8217BBB47 FOREIGN KEY (person_id) REFERENCES person (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE person_nationality ADD CONSTRAINT FK_E72ED4F81C9DA55 FOREIGN KEY (nationality_id) REFERENCES nationality (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE person_field ADD CONSTRAINT FK_F7C35A9C217BBB47 FOREIGN KEY (person_id) REFERENCES person (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE person_field ADD CONSTRAINT FK_F7C35A9C443707B0 FOREIGN KEY (field_id) REFERENCES field (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE person_scientific_council ADD CONSTRAINT FK_C6760C0F217BBB47 FOREIGN KEY (person_id) REFERENCES person (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE person_scientific_council ADD CONSTRAINT FK_C6760C0F142CCA8E FOREIGN KEY (scientific_council_id) REFERENCES scientific_council (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE postal_address ADD CONSTRAINT FK_972EFBF7217BBB47 FOREIGN KEY (person_id) REFERENCES person (id)');
        $this->addSql('ALTER TABLE postal_address ADD CONSTRAINT FK_972EFBF7F92F3E70 FOREIGN KEY (country_id) REFERENCES country (id)');
        $this->addSql('ALTER TABLE project ADD CONSTRAINT FK_2FB3D0EE9D1516C5 FOREIGN KEY (main_field_id) REFERENCES field (id)');
        $this->addSql('ALTER TABLE project ADD CONSTRAINT FK_2FB3D0EE217BBB47 FOREIGN KEY (person_id) REFERENCES person (id)');
        $this->addSql('ALTER TABLE role_person ADD CONSTRAINT FK_9FFA30C7D60322AC FOREIGN KEY (role_id) REFERENCES role (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE role_person ADD CONSTRAINT FK_9FFA30C7217BBB47 FOREIGN KEY (person_id) REFERENCES person (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE evaluation DROP FOREIGN KEY FK_1323A575B4489E4E');
        $this->addSql('ALTER TABLE application_form DROP FOREIGN KEY FK_A56495C750A89B2C');
        $this->addSql('ALTER TABLE nationality DROP FOREIGN KEY FK_8AC58B70F92F3E70');
        $this->addSql('ALTER TABLE postal_address DROP FOREIGN KEY FK_972EFBF7F92F3E70');
        $this->addSql('ALTER TABLE person_field DROP FOREIGN KEY FK_F7C35A9C443707B0');
        $this->addSql('ALTER TABLE project DROP FOREIGN KEY FK_2FB3D0EE9D1516C5');
        $this->addSql('ALTER TABLE person_nationality DROP FOREIGN KEY FK_E72ED4F81C9DA55');
        $this->addSql('ALTER TABLE evaluation DROP FOREIGN KEY FK_1323A575C5568CE4');
        $this->addSql('ALTER TABLE person_nationality DROP FOREIGN KEY FK_E72ED4F8217BBB47');
        $this->addSql('ALTER TABLE person_field DROP FOREIGN KEY FK_F7C35A9C217BBB47');
        $this->addSql('ALTER TABLE person_scientific_council DROP FOREIGN KEY FK_C6760C0F217BBB47');
        $this->addSql('ALTER TABLE postal_address DROP FOREIGN KEY FK_972EFBF7217BBB47');
        $this->addSql('ALTER TABLE project DROP FOREIGN KEY FK_2FB3D0EE217BBB47');
        $this->addSql('ALTER TABLE role_person DROP FOREIGN KEY FK_9FFA30C7217BBB47');
        $this->addSql('ALTER TABLE application_form DROP FOREIGN KEY FK_A56495C7166D1F9C');
        $this->addSql('ALTER TABLE country DROP FOREIGN KEY FK_5373C96698260155');
        $this->addSql('ALTER TABLE role_person DROP FOREIGN KEY FK_9FFA30C7D60322AC');
        $this->addSql('ALTER TABLE call_app DROP FOREIGN KEY FK_9032DC24142CCA8E');
        $this->addSql('ALTER TABLE person_scientific_council DROP FOREIGN KEY FK_C6760C0F142CCA8E');
        $this->addSql('DROP TABLE application_form');
        $this->addSql('DROP TABLE ask_contact');
        $this->addSql('DROP TABLE ask_login');
        $this->addSql('DROP TABLE call_app');
        $this->addSql('DROP TABLE country');
        $this->addSql('DROP TABLE evaluation');
        $this->addSql('DROP TABLE field');
        $this->addSql('DROP TABLE language');
        $this->addSql('DROP TABLE nationality');
        $this->addSql('DROP TABLE person');
        $this->addSql('DROP TABLE person_nationality');
        $this->addSql('DROP TABLE person_field');
        $this->addSql('DROP TABLE person_scientific_council');
        $this->addSql('DROP TABLE postal_address');
        $this->addSql('DROP TABLE project');
        $this->addSql('DROP TABLE region');
        $this->addSql('DROP TABLE role');
        $this->addSql('DROP TABLE role_person');
        $this->addSql('DROP TABLE scientific_council');
    }
}
