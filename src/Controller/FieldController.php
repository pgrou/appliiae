<?php

namespace App\Controller;

use App\Entity\Field;
use App\Form\Field1Type;
use App\Repository\FieldRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/field")
 */
class FieldController extends AbstractController
{
    /**
     * @Route("/", name="field_index", methods="GET")
     * @IsGranted("ROLE_ADMIN")
     */
    public function index(FieldRepository $fieldRepository): Response
    {
        return $this->render('field/index.html.twig', ['fields' => $fieldRepository->findAll()]);
    }

    /**
     * @Route("/new", name="field_new", methods="GET|POST")
     * @IsGranted("ROLE_ADMIN")
     */
    public function new(Request $request): Response
    {
        $field = new Field();
        $form = $this->createForm(Field1Type::class, $field);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($field);
            $em->flush();

            return $this->redirectToRoute('field_index');
        }

        return $this->render('field/new.html.twig', [
            'field' => $field,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="field_show", methods="GET")
     * @IsGranted("ROLE_ADMIN")
     */
    public function show(Field $field): Response
    {
        return $this->render('field/show.html.twig', ['field' => $field]);
    }

    /**
     * @Route("/{id}/edit", name="field_edit", methods="GET|POST")
     * @IsGranted("ROLE_ADMIN")
     */
    public function edit(Request $request, Field $field): Response
    {
        $form = $this->createForm(Field1Type::class, $field);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('field_index', ['id' => $field->getId()]);
        }

        return $this->render('field/edit.html.twig', [
            'field' => $field,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="field_delete", methods="DELETE")
     * @IsGranted("ROLE_ADMIN")
     */
    public function delete(Request $request, Field $field): Response
    {
        if ($this->isCsrfTokenValid('delete'.$field->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($field);
            $em->flush();
        }

        return $this->redirectToRoute('field_index');
    }
}
