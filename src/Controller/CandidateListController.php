<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

use App\Controller\UserInterface;
use App\Entity\ApplicationForm;
use App\Entity\Project;
use App\Entity\Person as Person;
use App\Entity\Role;
use App\Entity\AskLogin;
use App\Entity\Scientific_council;
use App\Entity\AskContact;
use App\Entity\Region;
use App\Entity\Field;
use App\Entity\Language;
use App\Entity\Country;
use App\Entity\PostalAddress;
use App\Entity\Nationality;
use App\Entity\Evaluation;


class CandidateListController extends AbstractController
{

    
    /**
     * @Route("/candidate_list", name="candidate_list")
     */
    public function getCandidateList(): Response
    {
       /* $generator = \Faker\Factory::create();
        $populator = new Faker\ORM\Propel\Populator($generator);
        $populator->addEntity('Person', 100);
        $populator->addEntity('Role', 10);
        $populator->addEntity('AskLogin', 10);
        $populator->addEntity('Scientific_council', 10);
        $populator->addEntity('AskContact', 10);
        $populator->addEntity('Region', 10);
        $populator->addEntity('Field', 10);
        $populator->addEntity('Language', 10);

        $populator->addEntity('Project', 10);
        $populator->addEntity('Country', 10);
        $populator->addEntity('PostalAddress', 10);
        $populator->addEntity('Nationality', 10);


        $populator->addEntity('ApplicationForm', 10);
        $populator->addEntity('Evaluation', 10);

        

        $insertedPKs = $populator->execute();
*/ 
        $user = $this->getUser();
        $conn = $this->getDoctrine()->getManager()->getConnection();

        // Selected fields to summarize an application
    	$sql = '
        SELECT project_id, application_form.submitted_at, person.lastname, person.firstname,email,person_id, project.title,application_form.id
        FROM application_form
        INNER JOIN project ON application_form.project_id = project.id
        INNER JOIN person ON project.person_id=person.id
        ';

        if(!in_array("ROLE_ADMIN", $this->getUser()->getRoles())){
            $sql = $sql.'WHERE person.id='.$user->getId();
        }

    	$stmt = $conn->prepare($sql);
    	$stmt->execute();

    // returns an array of arrays (i.e. a raw data set)
    	$stm = $stmt->fetchAll(); 
/*
        $repository = $this->getDoctrine()->getRepository(ApplicationForm::class);

        $application = $repository->findAll();
        for application in $application {
            $project.pu->findAllApplicationForm());
        }
        $project = $application->findAllApplicationForm();
       // $person = $project->getPerson();
*/
        return $this->render('candidate_list/index.html.twig', ['persons'=>$stm]); 
    }

    /**
    * @Route("/application_show/{id}", name="application_show")
    */
    public function application_show($id): Response
    {
        // We want to see someone's application in detail , id= application id
        $repository = $this->getDoctrine()->getRepository(ApplicationForm::class);

        $application = $repository->find($id);
        $person = $application->getPerson();

       	
    	return $this->render('candidate_list/show.html.twig', [
            'person'=>$person, 
            'application'=>$application,
        ]);
    }
}
