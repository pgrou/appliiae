<?php

namespace App\Controller;

use App\Entity\AskContact;
use App\Entity\AskLogin;
use App\Entity\Person;
use App\Entity\PasswordUpdate;
use App\Form\AskContactType;
use App\Form\AskLoginType;
use App\Form\PersonType;
use App\Form\EmailResetType;
use App\Form\RegistrationType;
use App\Form\PasswordResetType;
use App\Form\PasswordUpdateType;
use App\Repository\AskLoginRepository;
use App\Repository\AskContactRepository;
use App\Repository\PersonRepository;

use ReCaptcha\ReCaptcha;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

use Doctrine\Common\Persistence\ObjectManager;

use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class SecurityController extends AbstractController
{
    public static $email_sender = 'admission@iea-nantes.fr';
    private static $email_iea_notified;
    
    public function __construct(string $email_iea_notified)
    {
       //Get parameter from .env file
       if ($email_iea_notified == "")
           self::$email_iea_notified = "quentin.roques@iea-nantes.fr";
       else
           self::$email_iea_notified = $email_iea_notified;
    }

    /**
     * @Route("/registration", name="security_registration")
     */
    public function registration(Request $request, ObjectManager $manager, UserPasswordEncoderInterface $encoder, AskLoginRepository $askLoginRepository, \Swift_Mailer $mailer)
    {
        $asklogin= new AskLogin();
        $personbis = new Person();
        $form = $this->createForm(RegistrationType::class, $asklogin);
        
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $isSuccess = true;
            
            /*$recaptcha = new ReCaptcha('6LegDX0UAAAAAESiA5dTJddKjPltfc9RWyo4H0dj');
            $resp = $recaptcha->verify($request->request->get('g-recaptcha-response'), $request->getClientIp());
            if (!$resp->isSuccess()) {
                $isSuccess = false;
                $errorCodes = '';
                foreach($resp->getErrorCodes() as $code) { 
                    $errorCodes.=$code . ', '; 
                }
                // Do something if the submit wasn't valid ! Use the message to show something
                $this->addFlash(
                    'danger',
                    "The reCAPTCHA wasn't entered correctly. Go back and try it again.");
            }*/
            if(true){}
            else {
                $email = $asklogin->getEmail();
                $person = $askLoginRepository->findByEmail($email);
                if ($person != null) {
                    // Already asked
                    $isSuccess = false;
                    $errorCodes = '';
                    $this->addFlash(
                    'danger',
                    "Email already used.");
                }
            }
            
            if ($isSuccess) {
                // Everything works good ;) your contact has been saved.

                $personbis->setLastname($asklogin->getLastname());
                $personbis->setFirstname($asklogin->getFirstname());
                $personbis->setEmail($asklogin->getEmail());
                $personbis->setPassword($asklogin->getPassword());
                $personbis->setCreatedAt($asklogin->getCreatedAt());

                $hash = $encoder->encodePassword($asklogin, $asklogin->getPassword());
                $asklogin->setPassword($hash);
                $asklogin->setCreatedAt(new \DateTime());
                $asklogin->setGrpdConsentAt(new \DateTime());
                $manager->persist($asklogin);
                $manager->flush(); 

                $hash = $encoder->encodePassword($personbis, $personbis->getPassword());
                $personbis->setPassword($hash);
                $personbis->setCreatedAt(new \DateTime());
                $personbis->setGrpdConsentAt(new \DateTime());
                $manager->persist($personbis);
                $manager->flush();

                
                $this->addFlash(
                    'success',
                    "Your login request has been registered. You will be notified shortly by mail when you can login. ");
                
                // User notification
                $message = (new \Swift_Message('[IAS Nantes] Login request'))
                    ->setFrom(self::$email_sender)
                    ->setTo($asklogin->getEmail())
                    ->setBody(
                        $this->renderView(
                            // templates/emails/signin_notification.html.twig
                            'emails/signin_notification.html.twig',
                            array('person' => $asklogin)
                        ),
                        'text/html'
                    )
                ;
                $mailer->send($message);   
                
                // Admin notification
                $message = (new \Swift_Message('[IAS Nantes] New login request from '.$asklogin->getEmail()))
                    ->setFrom(self::$email_sender)
                    ->setTo(array(self::$email_iea_notified,'quentin.roques@iea-nantes.fr'))
                    ->setBody(
                        $this->renderView(
                            // templates/emails/signin_received.html.twig
                            'emails/signin_received.html.twig',
                            array('person' => $asklogin)
                        ),
                        'text/html'
                    )
                ;
                $mailer->send($message);
                
                return $this->redirectToRoute('security_login');
            }
        }
        return $this->render('security/registration.html.twig', [
            'form'=>$form->createView()
        ]);
    }
    /**
     * @Route("/contact", name="contact")
     */
    public function contact(Request $request, ObjectManager $manager, \Swift_Mailer $mailer)
    {
        $askcontact= new AskContact();
        $form = $this->createForm(AskContactType::class, $askcontact);
        
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $isSuccess = true;
            
            $recaptcha = new ReCaptcha('6LegDX0UAAAAAESiA5dTJddKjPltfc9RWyo4H0dj');
            $resp = $recaptcha->verify($request->request->get('g-recaptcha-response'), $request->getClientIp());
            if (!$resp->isSuccess()) {
                $isSuccess = false;
                $errorCodes = '';
                foreach($resp->getErrorCodes() as $code) { 
                    $errorCodes.=$code . ', '; 
                }
                // Do something if the submit wasn't valid ! Use the message to show something
                $this->addFlash(
                    'danger',
                    "The reCAPTCHA wasn't entered correctly. Go back and try it again.");
            }
            
            if ($isSuccess) {
                // Everything works good ;) your contact has been saved.

                $askcontact->setCreatedAt(new \DateTime());
                $manager->persist($askcontact);
                $manager->flush();
                
                $this->addFlash(
                    'success',
                    "Your demand has been sent.");
                
                // User notification
                $message = (new \Swift_Message('[IAS Nantes] contact'))
                    ->setFrom(self::$email_sender)
                    ->setTo($askcontact->getEmail())
                    ->setBody(
                        $this->renderView(
                            // templates/emails/signin_notification.html.twig
                            'emails/contact_notification.html.twig',
                            array('person' => $askcontact)
                        ),
                        'text/html'
                    )
                ;
                $mailer->send($message);   
                
                // Admin notification
                $message = (new \Swift_Message('[IAS Nantes] New contact from '.$askcontact->getEmail()))
                    ->setFrom(self::$email_sender)
                    ->setTo(self::$email_iea_notified)
                    ->setBody(
                        $this->renderView(
                            // templates/emails/signin_received.html.twig
                            'emails/contact_received.html.twig',
                            array('person' => $askcontact)
                        ),
                        'text/html'
                    )
                ;
                $mailer->send($message);
                
                return $this->redirectToRoute('homepage');
                return $this->redirectToRoute('security_login');
            }
        }
        return $this->render('security/contact.html.twig', [
            'form'=>$form->createView()
        ]);
    }
    
    /**
     * @Route("/login", name="security_login")
     */
    public function login(AuthenticationUtils $authenticationUtils){
         $error = $authenticationUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();
        return $this->render('security/login.html.twig', array(
            'last_username' => $lastUsername,
            'error'         => $error,
        ));
    }
    
    /**
     * @Route("/logout", name="security_logout")
     */
    public function logout(){
        //handled by security provider
    }

    /**
     * To edit user own personal information
     * @Route("/account/personal_information", name="personal_information")
     * @IsGranted("ROLE_USER")
     * @return Response
     */
    public function personal_information(Request $request, ObjectManager $manager){
        $user = $this->getUser();
        $form =$this->createForm(PersonType::class, $user);
        
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $manager->persist($user);
            $manager->flush();
            $this->addFlash(
                'success',
                "Modifications saved"
            );
        }
        return $this->render('security/personal_information.html.twig', [
            'form'=>$form->createView()
        ]);
    }

    /**
     * Update password
     * @Route("/account/update_password", name="update_password")
     * @IsGranted("ROLE_USER")
     * @return Response
     */
    public function updatePassword(Request $request, UserPasswordEncoderInterface $encoder, ObjectManager $manager ){
        $passwordUpdate = new PasswordUpdate();
        $person = $this->getUser();
        $form = $this->createForm(PasswordUpdateType::class, $passwordUpdate);
        
        $form->handleRequest($request);
        
        if($form->isSubmitted() && $form->isValid()){
            //Test if old password is correct
            if(!password_verify($passwordUpdate->getOldPassword(), $person->getPassword())){
                $form->get('oldPassword')->addError(new FormError("Incorrect password"));
            }else{  //Old password is correct
                $newPassword = $passwordUpdate->getNewPassword();
                $hash = $encoder->encodePassword($person, $newPassword);
                $person->setPassword($hash);
                $manager->persist($person);
                $manager->flush();
                $this->addFlash(
                    'success',
                    "Your password has been updated."
                );
                return $this->redirectToRoute('homepage');
            }
        }
        return $this->render('security/update_password.html.twig',[
            'form'=>$form->createView()
        ]);
    }

    /**
     * Undocumented function
     *
     * @Route("/forgotten_password", name="forgotten_passord")
     */
    public function forgottenPassword(Request $request, PersonRepository $personRepository, ObjectManager $manager, \Swift_Mailer $mailer){
        $form = $this->createForm(EmailResetType::class);

        $form->handleRequest($request, $manager);
        if($form->isSubmitted() && $form->isValid()){
            $person=$personRepository->findOneByEmail($form->get('email')->getData());
            //Test if email exist in database
            if(isset($person)){
                //TODO : find better random token => Finally, it seems ok according to StackOverflow. Still to check. 

                $person->setResetPasswordToken(bin2hex(openssl_random_pseudo_bytes(100)))
                    ->setTokenExpireAt((new \DateTime('now'))->modify('+1 day'));
                $manager->persist($person);
                $manager->flush();

                //Send email with link to reset password
                $message = (new \Swift_Message('[IAS Nantes] Reset password'))
                    ->setFrom('admission@iea-nantes.fr')
                    ->setTo($person->getEmail())
                    ->setBody(
                        $this->renderView(
                            'emails/reset_password.html.twig',
                            array('person' => $person)
                        ),
                        'text/html'
                    )
                ;
                $mailer->send($message);
            }
            $this->addFlash(
                'success',
                'An email will be sent to reset your password'
            );
        }
        return $this->render('security/forgotten_password.html.twig',[
            'form'=>$form->createView()
        ]);
    }

    /**
     * Route to reset password for another user. 
     * @Route("/reset_user_password/{id}", name="reset_user_password")
     * @IsGranted("ROLE_ADMIN")
     * @param Person $person
     * @return void
     */
    public function resetUserPassword(Person $person, ObjectManager $manager, \Swift_Mailer $mailer){
        //TODO : factorize this part with forgottenPassword function
        if(isset($person)){
            $person->setResetPasswordToken(bin2hex(openssl_random_pseudo_bytes(100)))
                ->setTokenExpireAt((new \DateTime('now'))->modify('+1 day'));
            $manager->persist($person);
            $manager->flush();

            //Send email with link to reset password
            $message = (new \Swift_Message('[IAS Nantes] Reset password'))
                ->setFrom('admission@iea-nantes.fr')
                ->setTo($person->getEmail())
                ->setBody(
                    $this->renderView(
                        'emails/reset_password.html.twig',
                        array('person' => $person)
                    ),
                    'text/html'
                )
            ;
            $mailer->send($message);
            $this->addFlash("success", 'An email will be send in few minutes to '.$person->getFullname().' to reset his/her password');
        }
        return $this->redirectToRoute('person_index');
    }

    /**
     * @Route("/reset_password/{id}/{token}", name="reset_password")
     *
     * @return void
     */
    public function resetPassword(Request $request, Person $person, ObjectManager $manager, UserPasswordEncoderInterface $encoder, $token){
        //Check if token in url is valid
        if($person->getResetPasswordToken()==$token && $person->getTokenExpireAt()>new \DateTime()){
            $passwordReset = new PasswordUpdate();
            
            $form = $this->createForm(PasswordResetType::class, $passwordReset);
            
            $form->handleRequest($request);
            if($form->isSubmitted() && $form->isValid()){
                $newPassword = $passwordReset->getNewPassword();
                $hash = $encoder->encodePassword($person, $newPassword);
                $person->setPassword($hash);
                $manager->persist($person);
                $manager->flush();
                // TODO : Check if user is admin or just user. 
                if($this->getUser() !== null && in_array("ROLE_ADMIN", $this->getUser()->getRoles())){
                    $this->addFlash(
                        'success',
                        "Password for user ". $person->getFullname()." has been updated."
                    );
                }else{
                    $this->addFlash(
                        'success',
                        "Your password has been updated."
                    );
                }
                return $this->redirectToRoute('security_login');
            }
            return $this->render('security/reset_password.html.twig',[
                'form'=>$form->createView(),

            ]);

        }else{
            $this->addFlash(
                'warning',
                "Requested page does not exist"
            );
            return $this->redirectToRoute('security_login');
        }
    }

}
