<?php

namespace App\Controller;

use App\Entity\ApplicationForm;
use App\Entity\Call;
use App\Entity\Person;
use App\Entity\Project;
use App\Form\AdministrativeType;
use App\Form\ApplicationFormType;
use App\Form\AttachementsType;
use App\Form\MotivationsType;
use App\Form\PersonType;
use App\Form\ProjectType;
use App\Form\SubmitApplicationType;
use App\Service\FileUploader;
use Cocur\Slugify\Slugify;
use Doctrine\Common\Persistence\ObjectManager;
use Dompdf\Dompdf;
use Dompdf\Options;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ApplicationFormController extends AbstractController
{
    public static $email_sender = 'admission@iea-nantes.fr';
	private static $email_iea_admission;
	
    public function __construct(string $email_iea_admission)
    {
        //Get parameter from .env file
        if ($email_iea_admission == "")
	        self::$email_iea_admission = "quentin.roques@iea-nantes.fr";
        else
	        self::$email_iea_admission = $email_iea_admission;
    }
    /**
     * Initiate a new application form
     *
     * @return Response
     * @Route("/call/{id}/apply", name="initializeApplication")
     * @Security("is_granted('ROLE_USER') and call.getIsOpen()")
     */
    public function initializeApplication(Call $call, Request $request, ObjectManager $manager){
        $user = $this->getUser();

        //Test if current user already applied to this call
        if($call->isCandidate($user)){
            $this->addFlash('warning', "You already applied to this call. Here is your application.");
            return $this->redirectToRoute('application_show', array('id'=>$call->isCandidate($user)->getId())); //TODO : Redirect to another page ('Edit' if not submitted and 'Show' otherwise)

        }else{ // Where current user didn't applied yet to this call
            $applicationForm = new ApplicationForm();
            $project = new Project();
            $form = $this->createForm(ProjectType::class,$project);

            $form->handleRequest($request);        
            if($form->isSubmitted() && $form->isValid()){
                $project->setPerson($user);
                $manager->persist($project);
                $applicationForm->setProject($project);
                $applicationForm->setCall($call);
                $manager->persist($applicationForm);
                $manager->flush();
                $this->addFlash(
                    'success',
                    "Modifications saved."
                );
                return $this->redirectToRoute('apply_2_personal_information',array('id'=> $applicationForm->getId()));
            }
            return $this->render('application_form/apply.html.twig', [
                'form' => $form->createView(),
                'call' => $call,
                ]);
        }
    }
    /**
     * Initiate a new application form for someone else from connected user
     *
     * @return Response
     * @Route("/call/{id}/create_application", name="createApplication")
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function createApplication(Call $call, Request $request, ObjectManager $manager)
    {
         //Test if current user already applied to this call
         $applicationForm = new ApplicationForm();
         $project = new Project();
         $form = $this->createForm(ApplicationFormType::class,$applicationForm);
         
         $form->handleRequest($request);        
         if($form->isSubmitted() && $form->isValid()){
             
            $candidate = $form->get('candidate')->getData();
            if($call->isCandidate($candidate)){
                $this->addFlash('warning', "This person already has an application for this call. Here is the application.");
                return $this->redirectToRoute('application_show', array('id'=>$call->isCandidate($candidate)->getId()));
    
            } // Case when candidate didn't applied yet to this call
            $project = $applicationForm->getProject();
            $project->setPerson($candidate);
            $manager->persist($project);
            $applicationForm->setCall($call);
            $manager->persist($applicationForm);
            $manager->flush();
            $this->addFlash(
                'success',
                "Modifications saved."
            );
            return $this->redirectToRoute('apply_2_personal_information',array('id'=> $applicationForm->getId()));
        }
        return $this->render('application_form/createApplication.html.twig', [
            'form' => $form->createView(),
            'call' => $call,
            ]);

    }
    /**
     * first step for application
     * @Route("/application/{id}/project", name="apply_1_project")
     * @Security("is_granted('ROLE_USER') and application.getCall().getIsOpen() and application.getProject().getPerson() === user or is_granted('ROLE_ADMIN')")
     */
    public function editProject(ApplicationForm $application, Request $request, ObjectManager $manager){
        $check =$this->checkApplication($application, $request);
        if($check !== null){
            return $check;
        }        
        $project = $application->getProject();
        $form = $this->createForm(ProjectType::class,$project);
      
        $form->handleRequest($request);        
        if($form->isSubmitted() && $form->isValid()){
            $manager->persist($project);
            $manager->flush();
            $this->addFlash(
                'success',
                "Modifications saved."
            );
            return $this->redirectToRoute('apply_2_personal_information', array('id'=> $application->getId()));
        }
        return $this->render('application_form/generic_form.html.twig', [
            'form' => $form->createView(),
            'application' => $application,
            'title'=> 'Project',
            'previous_route'=>'homepage',
            ]);
    }
    /**
     * Second step for application
     * @Route("/application/{id}/personal_information", name="apply_2_personal_information")
     * @Security("is_granted('ROLE_USER') and application.getCall().getIsOpen() and application.getProject().getPerson() === user or is_granted('ROLE_ADMIN')")
     */
    public function editPersonalInformations(ApplicationForm $application, Request $request, ObjectManager $manager){
        $check =$this->checkApplication($application, $request);
        if($check !== null){
            return $check;
        }        
        $user = $application->getProject()->getPerson();
        $form = $this->createForm(PersonType::class,$user);
        $form->handleRequest($request);        
        if($form->isSubmitted() && $form->isValid()){
            $manager->persist($user);
            $manager->flush();
            $this->addFlash(
                'success',
                "Modifications saved."
            );
            return $this->redirectToRoute('apply_3_motivations', array('id'=> $application->getId()));
        }
        return $this->render('application_form/generic_form.html.twig', [
            'form' => $form->createView(),
            'application' => $application,
            'title'=> 'Personal information',
            'previous_route'=>'apply_1_project',
        ]);
    }
    /**
     * third step for application
     * @Route("/application/{id}/motivations", name="apply_3_motivations")
     * @Security("is_granted('ROLE_USER') and application.getCall().getIsOpen() and application.getProject().getPerson() === user or is_granted('ROLE_ADMIN')")
     */
    public function editMotivations(ApplicationForm $application, Request $request, ObjectManager $manager){
        $check =$this->checkApplication($application, $request);
        if($check !== null){
            return $check;
        }      
        $form = $this->createForm(MotivationsType::class,$application);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $manager->persist($application);
            $manager->flush();
            $this->addFlash(
                'success',
                "Modifications saved."
            );
            return $this->redirectToRoute('apply_4_administrative', array('id'=>$application->getId()));
        }
        return $this->render('application_form/generic_form.html.twig', [
            'form' => $form->createView(),
            'application' => $application,
            'title'=> 'Motivations',
            'previous_route'=>'apply_2_personal_information',
            ]);
    }
    /**
     * fourth step for application
     * @Route("/application/{id}/administrative", name="apply_4_administrative")
     * @Security("is_granted('ROLE_USER') and application.getCall().getIsOpen() and application.getProject().getPerson() === user or is_granted('ROLE_ADMIN')")
     */
    public function editAdministrative(ApplicationForm $application, Request $request, ObjectManager $manager){
        $check =$this->checkApplication($application, $request);
        if($check !== null){
            return $check;
        }        
        $form = $this->createForm(AdministrativeType::class,$application);
        $form->handleRequest($request);        
        if($form->isSubmitted() && $form->isValid()){
            $manager->persist($application);
            $manager->flush();
            $this->addFlash(
                'success',
                "Modifications saved."
            );
            return $this->redirectToRoute('apply_5_attachements', array('id'=> $application->getId()));
        }
        return $this->render('application_form/generic_form.html.twig', [
            'form' => $form->createView(),
            'application' => $application,
            'title'=> 'Administrative information',
            'previous_route'=>'apply_3_motivations',
            ]);
    }

    /**
     * @Route("/application/{id}/attachements", name="apply_5_attachements")
     * @Security("is_granted('ROLE_USER') and application.getCall().getIsOpen() and application.getProject().getPerson() === user or is_granted('ROLE_ADMIN')")
     */
    public function editAttachement(ApplicationForm $application, Request $request, ObjectManager $manager){
        $check =$this->checkApplication($application, $request);
        if($check !== null){
            return $check;
        }    
    
        $form = $this->createForm(AttachementsType::class,$application);
        $form->handleRequest($request);
        //Slug used to avoid spac or special characters into filename
        $slugify = new Slugify();
        $name = $application->getPerson()->getLastnameSlug();
        $attachements = [];
        $attachements['Curriculum Vitae'] = array(
            'subFolderId'=>$application->getId(),
            'filename'=>$application->getFileCurriculumVitae(),
            'finalFilename'=>'2-'.$name.'-CV',
            'formfield'=>'formCurriculumVitae',
            'setter'=>'setFileCurriculumVitae',
            'type'=>'ApplicationForm',
        );
        $attachements['Project'] = array(
            'subFolderId'=>$application->getProject()->getId(),
            'comment'=>'Explications',
            'filename'=>$application->getProject()->getFileProject(),
            'finalFilename'=>'3-'.$name.'-Projet',
            'formfield'=>'formProject',
            'setter'=>'setFileProject',
            'type'=>'Project',
        );
        $attachements['Publication 1'] = array(
            'subFolderId'=>$application->getProject()->getId(),
            'filename'=>$application->getProject()->getFilePublication1(),
            'finalFilename'=>'4-'.$name.'-Publication1',
            'formfield'=>'formPublication1',
            'setter'=>'setFilePublication1',
            'type'=>'Project',
        );
        $attachements['Publication 2'] = array(
            'subFolderId'=>$application->getProject()->getId(),
            'filename'=>$application->getProject()->getFilePublication2(),
            'finalFilename'=>'5-'.$name.'-Publication2',
            'formfield'=>'formPublication2',
            'setter'=>'setFilePublication2',
            'type'=>'Project',
        );
        
        if($form->isSubmitted() && $form->isValid()){
        	$isAllUploaded = true;
        	$Message = "";
            foreach ($attachements as $attachementName => $attachement){
                $file = $form->get($attachement['formfield'])->getData();
                if(isset($file)){
                    /** @var Symfony\Component\HttpFoundation\File\UploadedFile $file */
                    $fileName = $attachement['finalFilename'].'.'.$file->guessExtension();
                    if (strtolower($file->guessExtension()) != "pdf") {
						$isAllUploaded = false;
						$Message .= "<br/>$fileName is not a PDF file";
                    } else if (filesize($file) >= 41943040) {
						$isAllUploaded = false;
						$Message .= "<br/>$fileName is too large";
                    } else {
						try {
							$file->move(
								$this->getParameter('upload_directory').'/'.$attachement['type'].'/'.$attachement['subFolderId'],
								$fileName
							);
							$setter = $attachement['setter'];
							if($attachement['type']=='ApplicationForm'){
								$application->$setter($fileName);
							}elseif($attachement['type']=='Project'){
								$application->getProject()->$setter($fileName);
							}
						} catch (FileException $e) {
							$isAllUploaded = false;
							$Message .= "<br/>$fileName upload problem";
						}
                    }
                }
            }

            if (! $isAllUploaded) {
            	// If at least 1 crashed
				$this->addFlash(
					'danger',
					'An error occured during upload. Please try again or contact our services.'.$Message
				);
				return $this->render('application_form/attachements_form.html.twig', [
					'form' => $form->createView(),
					'application' => $application,
					'title'=> 'Attachements',
					'previous_route'=>'apply_3_motivations',
            		'attachements'=>$attachements,
					]);
            } else {
				$manager->persist($application);
				$manager->persist($application->getProject());
				$manager->flush();
				$this->addFlash(
					'success',
					"Modifications saved."
				);
				return $this->redirectToRoute('application_review', array('id'=> $application->getId()));
            }
        }
        return $this->render('application_form/attachements_form.html.twig', [
            'form' => $form->createView(),
            'application' => $application,
            'title'=> 'Attachements',
            'previous_route'=>'apply_4_administrative',
            'attachements'=>$attachements,
            ]);
    }
    
    private function processReviewApplicationForm(ApplicationForm $application, Request $request, ObjectManager $manager, \Swift_Mailer $mailer, bool $submitted) {
    	if ($submitted) {
	        $check =$this->checkApplication($application, $request);
    	    if($check !== null){
        	    return $check;
        	}
        }
        
        $formProject = $this->createForm(
            ProjectType::class, 
            $application->getProject(), 
            array(
                'disabled'=> true,
            )
        );
        $formPers = $this->createForm(
            PersonType::class,
            $application->getProject()->getPerson(),
            array(
                'disabled'=> true,
            )
        );
        $formMotiv = $this->createForm(
            MotivationsType::class, 
            $application,
            array(
                'disabled'=> true,
            )
        );
        $formAdmin = $this->createForm(
            AdministrativeType::class, 
            $application,
            array(
                'disabled'=> true,
            )
        );
        $formAttachement = $this->createForm(
            AttachementsType::class, 
            $application,
            array(
                'disabled'=> true,
            )
        );
    	if ($submitted) {
			$formSubmit = $this->createForm(
				SubmitApplicationType::class
			);
			$formSubmit->handleRequest($request);
		} else {
            $formSubmit = $this->createForm(
                SubmitApplicationType::class,
                $application,
                array(
                    'disabled'=>true,
                )
            );
		}
        
        $errorList = array();
    	if ($submitted) {
			if($formSubmit->isSubmitted() && $formSubmit->isValid()) {
				$errorList = $application->checkValidity();
			
				if (count($errorList) == 0) {
					$application->setSubmittedAt(new \DateTime());
					$manager->persist($application);
					$manager->flush();
					$this->generatePdf($application);
					$this->addFlash(
						'success',
						"Your application has been submitted with success."
					);
					$this->sendCandidateNotification($application, $mailer);
					$this->sendAdmissionNotification($application, $mailer);
					return $this->redirectToRoute('homepage');
				}
			}
		}

		$parameters = [
            'formProject'       => $formProject->createView(),
            'formPers'          => $formPers->createView(),
            'formMotiv'         => $formMotiv->createView(),
            'formAdmin'         => $formAdmin->createView(),
            'formAttachement'   => $formAttachement->createView(),
            'formSubmit'        => $formSubmit->createView(),
            'application'       => $application,
            'errorList'			=> $errorList,
            'title'				=> 'Review'
            ];
    	if ($submitted) {
    		$parameters['previous_route'] = 'apply_5_attachements';
		}
		        
        return $this->render('application_form/review.html.twig', $parameters);
    }
    
    /**
     * final step for application
     * @Route("/application/{id}/review", name="application_review")
     * @Security("is_granted('ROLE_USER') and application.getProject().getPerson() === user or is_granted('ROLE_ADMIN')")
     */
    public function reviewApplication(ApplicationForm $application, Request $request, ObjectManager $manager, \Swift_Mailer $mailer){
        return $this->processReviewApplicationForm($application, $request, $manager, $mailer, true);
    }

    /**
     * Read only view for an application
     *
     * @Route("/application/{id}/show", name="application_show")
     * @Security("is_granted('ROLE_USER') and application.getProject().getPerson() === user or is_granted('ROLE_ADMIN')")
     */
    public function showApplication(ApplicationForm $application, Request $request, ObjectManager $manager, \Swift_Mailer $mailer) {
        return $this->processReviewApplicationForm($application, $request, $manager, $mailer, false);
    }

	/**
	 * This route can be called to regenerate PDF file. 
	 * @Route("/application/{id}/generatePdf", name="application_generatePdf")
	 * @Security("is_granted('ROLE_ADMIN')")
	 */
	public function regeneratePdf(ApplicationForm $application){
		$this->generatePdf($application);
		$path = $this->getParameter('upload_directory').'/ApplicationForm/'.$application->getId().'/';
		
		$filename='1-'.$application->getPerson()->getLastnameSlug().'-Formulaire.pdf';
		return new BinaryFileResponse($path.$filename);
	}
	
	/**
	 * Method to check if application can still be modified. 
	 * If applicant already submitted this application, user get redirected to read-only view. 
	 * If call is closed, user get redirected to read-only view. 
	 *
	 * @return void
	 */
	public function checkApplication(ApplicationForm $application, Request $request)
	{
		//If already submitted, user get redirected to application_show route.
		if($application->getSubmittedAt()!= null){
			$this->addFlash(
				'info',
				"This application has already been submitted. You can't modify it anymore."
			);
			return $this->redirectToRoute('application_show', array('id'=>$application->getId()));
        }
        //If connected user is admin
        if(in_array('ROLE_ADMIN' , $this->getUser()->getRoles())){
            $this->addFlash(
				'warning',
				"This call is now closed, but as admin you can still modify some information."
            );
            //return $this->redirectToRoute('application_review', array('id'=>$application->getId()));
            return null;
        }

        //If call is closed, user get redirected to application_show route.
		if(!$application->getCall()->getIsOpen()){
			$this->addFlash(
				'warning',
				"This call is now closed, you can't edit/submit this application."
			);
			return $this->redirectToRoute('application_show', array('id'=>$application->getId()));
		}
	}
  
	/**
	 * @return string
	 */
	private function generateUniqueFileName()
	{
		// md5() reduces the similarity of the file names generated by
		// uniqid(), which is based on timestamps
		return md5(uniqid());
	}

	/**
	 * QRO - Function to generate a pdf file and store it on application folder. 
	 *
	 * @param ApplicationForm $application
	 * @return void
	 */
	private function generatePdf(ApplicationForm $application)
	{
		$manager = $this->getDoctrine()->getManager();
		// PDF generation :
		$pdfOptions = new Options();
		$pdfOptions->set('defaultFont', 'DejaVu Sans');
		// Instantiate Dompdf with options
		$dompdf = new Dompdf($pdfOptions);
		$dompdf->set_option("isPhpEnabled", true);
		
		// Retrieve the HTML generated in twig file
		$html = $this->renderView('pdf/applicationFrom.html.twig', [
			'application' => $application
		]);
		// Load HTML to Dompdf
		$dompdf->loadHtml($html);
				
		// (Optional) Setup the paper size and orientation 'portrait' or 'portrait'
		$dompdf->setPaper('A4', 'portrait');

		// Render the HTML as PDF
		$dompdf->render();
		$output = $dompdf->output();

		$pdfFilepath = $this->getParameter('upload_directory').
							'/ApplicationForm/'.
							$application->getId().'/';
		if(!is_dir($pdfFilepath)){ // QRO - If path doesn't exist make it. 
			mkdir($pdfFilepath);
			//TODO : Add exception handling if mkdir() fail.
		}
		$pdfFilename = '1-'.
							$application->getPerson()->getLastnameSlug().
							'-Formulaire.pdf';
		// Write file to the desired path
		file_put_contents($pdfFilepath.$pdfFilename, $output);
		$application->setFileApplicationForm($pdfFilename);
		$manager->persist($application);
		$manager->flush();
	}

	/**
	 * Route to manually re-send notification mail to admission
	 * @Route("/application/{id}/sendAdmissionNotification", name="send_admission_notification")
	 * @Security("is_granted('ROLE_ADMIN')")
	 * 
	 */
	public function reSendAdmissionNotification(ApplicationForm $application, \Swift_Mailer $mailer, Request $request){
		$this->sendAdmissionNotification($application, $mailer);
		$this->addFlash(
			'success', 
			"Notification has been re-sent to admission"
		);
		return $this->redirectToRoute('call_show', array('id'=>$application->getCall()->getId()));
	}
	/**
	 * QRO - Private function to send Admission notification when an application is submitted. 
	 *
	 * @param ApplicationForm $application
	 * @param \Swift_Mailer $mailer
	 * @return void
	 */
	private function sendAdmissionNotification(ApplicationForm $application, \Swift_Mailer $mailer){
		// Admin notification
		$message = (new \Swift_Message('[IAS Nantes] New application submitted by '.$application->getPerson()->getFullname()))
        ->setFrom(self::$email_sender)
        ->setCc('quentin.roques@iea-nantes.fr')
		->setTo(self::$email_iea_admission)
		->setBody(
			$this->renderView(
				// templates/emails/application_submitted.html.twig
				'emails/application_submitted.html.twig',
				array('application' => $application)
			),
			'text/html'
		);
		$mailer->send($message);
	}
	/**
	 * Route to manually re-send notification mail to canditate
	 * @Route("/application/{id}/sendCandidateNotification", name="send_candidate_notification")
	 * @Security("is_granted('ROLE_ADMIN')")
	 * 
	 */
	public function reSendCandidateNotification(ApplicationForm $application, \Swift_Mailer $mailer, Request $request){
		$this->sendCandidateNotification($application, $mailer);
		$this->addFlash(
			'success', 
			"Notification has been re-sent to candidate"
		);
		return $this->redirectToRoute('call_show', array('id'=>$application->getCall()->getId()));
	}
	/**
	 * QRO - Private function to send candidate notifiaction when an application is submitted. 
	 *
	 * @param ApplicationForm $application
	 * @param \Swift_Mailer $mailer
	 * @return void
	 */
	private function sendCandidateNotification(ApplicationForm $application, \Swift_Mailer $mailer){
		// Candidate notification
		$message = (new \Swift_Message('[IAS Nantes] Your application has been submitted'))
		->setFrom(self::$email_sender)
		->setBcc(array(self::$email_iea_admission,'quentin.roques@iea-nantes.fr'))
        ->setTo($application->getPerson()->getEmail())
        ->attach(\Swift_Attachment::fromPath($this->getParameter('upload_directory').'/ApplicationForm/'.$application->getId().'/'.$application->getFileApplicationForm()))
		->setBody(
			$this->renderView(
				// templates/emails/application_submitted.html.twig
				'emails/application_submitted_candidate.html.twig',
				array('application' => $application)
			),
			'text/html'
		);
		$mailer->send($message);
    }
    
    /**
     * @Route("/myApplications", name="my_applications")
	 * @Security("is_granted('ROLE_USER')")
     *
     */
    public function myApplications(Request $request, ObjectManager $manager){
        $user = $this->getUser();
        return $this->render('application_form/my_applications.html.twig', [
            'person'=>$user
        ]);
    }
}
