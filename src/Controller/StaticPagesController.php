<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * This controller is use to redirect to static pages
 */
class StaticPagesController extends AbstractController
{
    /**
     * @Route("/legal-notices", name="legal_notices", methods="GET")
     */
    public function legalNotices(){
        return $this->render('static_pages/legal_notices.html.twig');
    }
    /**
     * @Route("/faq", name="faq", methods="GET")
     */
    public function faq(){
        return $this->render('static_pages/faq.html.twig');
    }
    /**
     * @Route("/how-to-apply", name="how_to_apply", methods="GET")
     */
    public function howToApply(){
        return $this->render('static_pages/how_to_apply.html.twig');
    }


}
