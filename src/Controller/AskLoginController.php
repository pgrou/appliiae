<?php

namespace App\Controller;

use App\Entity\AskLogin;
use App\Entity\Person;
use App\Entity\Project;
use App\Form\AskLoginType;
use App\Repository\AskLoginRepository;
use App\Repository\PersonRepository;
use App\Controller\SecurityController;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @IsGranted("ROLE_ADMIN")
 * @Route("/asklogin")
 */
class AskLoginController extends AbstractController
{
    /**
     * @Route("/", name="asklogin_index", methods="GET")
     * @IsGranted("ROLE_ADMIN")
     */
    public function index(AskLoginRepository $askLoginRepository): Response
    {
        return $this->render('asklogin/index.html.twig', ['people' => $askLoginRepository->findAllOrderedByStatus()]);
    }

    /**
     * @Route("/new", name="asklogin_new", methods="GET|POST")
     */
    public function new(Request $request, AskLoginRepository $askLoginRepository): Response
    {
        $asklogin = new AskLogin();
        $form = $this->createForm(AskLoginType::class, $asklogin);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
        	$email = $asklogin->getEmail();
        	$person = $askLoginRepository->findByEmail($email);
        	
        	if ($person == null) {
            	$em = $this->getDoctrine()->getManager();
            	$em->persist($asklogin);
            	$em->flush();

            	return $this->redirectToRoute('asklogin_index');
            }
        }

        return $this->render('asklogin/new.html.twig', [
            'asklogin' => $asklogin,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="asklogin_show", methods="GET")
     * @IsGranted("ROLE_ADMIN")
     */
    public function show(AskLogin $asklogin): Response
    {
        return $this->render('asklogin/show.html.twig', ['asklogin' => $asklogin]);
    }

    /**
     * @Route("/{id}/edit", name="asklogin_edit", methods="GET|POST")
     * @IsGranted("ROLE_ADMIN")
     */
    public function edit(Request $request, AskLogin $asklogin): Response
    {
        $form = $this->createForm(PersonType::class, $asklogin);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('asklogin_index', ['id' => $asklogin->getId()]);
        }

        return $this->render('asklogin/edit.html.twig', [
            'asklogin' => $asklogin,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="asklogin_delete", methods="DELETE")
     * @IsGranted("ROLE_ADMIN")
     */
    public function delete(Request $request, AskLogin $asklogin): Response
    {
        if ($this->isCsrfTokenValid('delete'.$asklogin->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($asklogin);
            $em->flush();
        }

        return $this->redirectToRoute('asklogin_index');
    }

    /**
     * @Route("/{id}/accept", name="asklogin_accept", methods="GET|POST")
     * @IsGranted("ROLE_ADMIN")
     */
    public function accept(Request $request, AskLogin $asklogin, AskLoginRepository $askLoginRepository, PersonRepository $personRepository, \Swift_Mailer $mailer): Response
    {
		$em = $this->getDoctrine()->getManager();
		
		$email = $asklogin->getEmail();
		$person = $personRepository->findByEmail($email);
		
		if ($person ==null) {
		// Create new person
			$person = new Person();
			$person->setLastname($asklogin->getLastname());
			$person->setFirstname($asklogin->getFirstname());
			$person->setUsualLastname($asklogin->getLastname());
			$person->setUsualFirstname($asklogin->getFirstname());
			$person->setEmail($asklogin->getEmail());
			$person->setPassword($asklogin->getPassword());
			$person->setConfirmPassword($asklogin->getPassword());
			$person->setCreatedAt(new \DateTime());
			$person->setGrpdConsentAt($asklogin->getGrpdConsentAt());
			$em->persist($person);
		}
		
		//Change status
		$asklogin->setStatus(2);

		// Create Project
		// $project = new Project();
		// $project->setTitle("Project Title");
		// $project->setSummary($asklogin->getProject());
		// $project->setPerson($person);
		// $em->persist($project);
		
		// Link project and person
		// $person->addProject($project);
		
		$em->flush();

		// User notification
		$message = (new \Swift_Message('[IAS Nantes] Login request accepted'))
			->setFrom(SecurityController::$email_sender)
			->setTo($asklogin->getEmail())
			->setBody(
				$this->renderView(
					// templates/emails/signin_notification.html.twig
					'emails/signin_accepted.html.twig',
					array('person' => $asklogin)
				),
				'text/html'
			)
		;
		$mailer->send($message);   
        
        return $this->redirectToRoute('asklogin_index');
    }

    /**
     * @Route("/{id}/refuse", name="asklogin_refuse", methods="GET|POST")
     * @IsGranted("ROLE_ADMIN")
     */
    public function refuse(Request $request, AskLogin $asklogin, AskLoginRepository $askLoginRepository, \Swift_Mailer $mailer): Response
    {
		$em = $this->getDoctrine()->getManager();
		//Change status
		$asklogin->setStatus(3);
		
		$em->flush();

		// User notification
		$message = (new \Swift_Message('[IAS Nantes] Login request refused'))
			->setFrom(SecurityController::$email_sender)
			->setTo($asklogin->getEmail())
			->setBody(
				$this->renderView(
					// templates/emails/signin_notification.html.twig
					'emails/signin_refused.html.twig',
					array('person' => $asklogin)
				),
				'text/html'
			)
		;
		$mailer->send($message);   

        return $this->redirectToRoute('asklogin_index');
    }
}
