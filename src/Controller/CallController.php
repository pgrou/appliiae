<?php

namespace App\Controller;

use App\Entity\Call;
use App\Form\CallType;
use App\Repository\CallRepository;
use App\Repository\ApplicationFormRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CallController extends AbstractController
{
    /**
     * 
     * @Route("/", name="homepage", methods="GET")
     * No "IsGranted" anotation because it should be accessible by anyone
     */
    public function homepage(CallRepository $callRepository): Response
    {   
        
        return $this->render('call/homepage.html.twig', [
            'calls' => $callRepository->findByOnGoing(),
            'upcomingCalls'=>$callRepository->findByUpcoming(),
        ]);
        
    }

    /**
     * @Route("/call/", name="call_index", methods="GET")
     * @IsGranted("ROLE_ADMIN")
     */
    public function index(CallRepository $callRepository, ApplicationFormRepository $applicationFormRepository): Response
    {
        return $this->render('call/index.html.twig', [
            'calls' => $callRepository->findBy([], ['closingDate' => 'DESC']),
            ]);
    }

    /**
     * @Route("/call/new", name="call_new", methods="GET|POST")
     * @IsGranted("ROLE_ADMIN")
     */
    public function new(Request $request): Response
    {
        $call = new Call();
        $form = $this->createForm(CallType::class, $call);
        $form->handleRequest($request);
        

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($call);
            $em->flush();

            return $this->redirectToRoute('call_index');
        }
        return $this->render('call/new.html.twig', [
            'call' => $call,    
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/call/{id}", name="call_show", methods="GET")
     * @IsGranted("ROLE_ADMIN")
     */
    public function show(Call $call, Request $request): Response
    {
        return $this->render('call/show.html.twig', [
            'call' => $call,
        ]);
    }

    /**
     * @Route("/call/{id}/edit", name="call_edit", methods="GET|POST")
     * @IsGranted("ROLE_ADMIN")
     */
    public function edit(Request $request, Call $call): Response
    {
        $form = $this->createForm(CallType::class, $call);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('call_index', ['id' => $call->getId()]);
        }

        return $this->render('call/edit.html.twig', [
            'call' => $call,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/call/{id}", name="call_delete", methods="DELETE")
     * @IsGranted("ROLE_ADMIN")
     */
    public function delete(Request $request, Call $call): Response
    {
        if ($this->isCsrfTokenValid('delete'.$call->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($call);
            $em->flush();
        }

        return $this->redirectToRoute('call_index');
    }

   
}
