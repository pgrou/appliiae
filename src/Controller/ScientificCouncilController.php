<?php

namespace App\Controller;

use App\Entity\ScientificCouncil;
use App\Form\ScientificCouncilType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Repository\ScientificCouncilRepository;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/scientific/council")
 * @Route("CS")
 */
class ScientificCouncilController extends AbstractController
{
    /**
     * @Route("/", name="scientific_council_index", methods="GET")
     * @IsGranted("ROLE_ADMIN")
     */
    public function index(ScientificCouncilRepository $scientificCouncilRepository): Response
    {
        return $this->render('scientific_council/index.html.twig', ['scientific_councils' => $scientificCouncilRepository->findAll()]);
    }

    /**
     * @Route("/new", name="scientific_council_new", methods="GET|POST")
     * @IsGranted("ROLE_ADMIN")
     */
    public function new(Request $request): Response
    {
        $scientificCouncil = new ScientificCouncil();
        $form = $this->createForm(ScientificCouncilType::class, $scientificCouncil);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            foreach($scientificCouncil->getCalls() as $call){
                $call->setScientificCouncil($scientificCouncil);
                $em->persist($call);
            }
            $em->persist($scientificCouncil);
            $em->flush();

            return $this->redirectToRoute('scientific_council_index');
        }

        return $this->render('scientific_council/new.html.twig', [
            'scientific_council' => $scientificCouncil,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="scientific_council_show", methods="GET")
     * @IsGranted("ROLE_ADMIN")
     */
    public function show(ScientificCouncil $scientificCouncil): Response
    {
        return $this->render('scientific_council/show.html.twig', ['scientific_council' => $scientificCouncil]);
    }

    /**
     * @Route("/{id}/edit", name="scientific_council_edit", methods="GET|POST")
     * @IsGranted("ROLE_ADMIN")
     */
    public function edit(Request $request, ScientificCouncil $scientificCouncil): Response
    {
        $form = $this->createForm(ScientificCouncilType::class, $scientificCouncil);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('scientific_council_index', ['id' => $scientificCouncil->getId()]);
        }

        return $this->render('scientific_council/edit.html.twig', [
            'scientific_council' => $scientificCouncil,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="scientific_council_delete", methods="DELETE")
     * @IsGranted("ROLE_ADMIN")
     */
    public function delete(Request $request, ScientificCouncil $scientificCouncil): Response
    {
        if ($this->isCsrfTokenValid('delete'.$scientificCouncil->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($scientificCouncil);
            $em->flush();
        }

        return $this->redirectToRoute('scientific_council_index');
    }
}
