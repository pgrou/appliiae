<?php

namespace App\Controller;

use ZipArchive;
use App\Entity\Person;
use App\Entity\Project;
use Cocur\Slugify\Slugify;
use App\Entity\ApplicationForm;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class DownloadController extends AbstractController
{
    /**
     * @Route("/download/Project/{id}/{filename}", name="download_Project", methods="GET")
     * @Security("is_granted('ROLE_USER') and project.getPerson() === user or is_granted('ROLE_ADMIN')")
     */
    public function download_project(Project $project, $filename)
    {
        $filePath=$this->getParameter('kernel.project_dir').'/uploads/Project/'.$project->getId().'/';
        
        $response = new BinaryFileResponse($filePath.$filename);
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            ''
        );        
        return $response;
        
    }

     /**
     * @Route("/download/ApplicationForm/{id}/{filename}", name="download_ApplicationForm", methods="GET")
     * @Security("is_granted('ROLE_USER') and applicationForm.getProject().getPerson() === user or is_granted('ROLE_ADMIN')")
     */
    public function download_application(ApplicationForm $applicationForm, $filename)
    {
            $filePath=$this->getParameter('kernel.project_dir').'/uploads/ApplicationForm/'.$applicationForm->getId().'/';
        
            $response = new BinaryFileResponse($filePath.$filename);
            $response->setContentDisposition(
                ResponseHeaderBag::DISPOSITION_ATTACHMENT,
                ''
            );        
        return $response;
    }

    /**
     * @Route("/download/ApplicationFormZip/{id}", name="download_ApplicationZip")
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function download_zip(ApplicationForm $applicationForm){
        
        $slugify = new Slugify();
        $files = $applicationForm->getAllFiles();
        $rootDir=$this->getParameter('kernel.project_dir');
        $zip = new \ZipArchive();
        $zipName = $applicationForm->getPerson()->getLastnameSlug()."_".ucfirst($slugify->slugify($applicationForm->getPerson()->getFirstname())).".zip";
        $zip->open($zipName,  \ZipArchive::CREATE);

        foreach ($files as $f) {
            
            if (file_exists($rootDir.$f) && is_file($rootDir.$f)){
                $new_filename = substr($f,strrpos($f,'/') + 1);
                $zip->addFile($rootDir.$f, $new_filename);
            }
            
        }
        $zip->close();
        $response = new BinaryFileResponse($zipName);
        $response->headers->set('Content-Type', 'application/zip');
        $response->headers->set('Content-Disposition', 'attachment;filename="' . $zipName . '"');
        $response->headers->set('Content-length', filesize($zipName));
        return $response;
    }

  
}
