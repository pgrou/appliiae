<?php

namespace App\Repository;

use App\Entity\ApplicationForm;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ApplicationForm|null find($id, $lockMode = null, $lockVersion = null)
 * @method ApplicationForm|null findOneBy(array $criteria, array $orderBy = null)
 * @method ApplicationForm[]    findAll()
 * @method ApplicationForm[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ApplicationFormRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ApplicationForm::class);
    }

    public function findBySubmitted(){      
        return $this->createQueryBuilder('c')
            ->andWhere('c.submittedAt is not null')
            ->getQuery()
            ->getResult()
        ;
    }

    public function findByUnsubmitted(){      
        return $this->createQueryBuilder('c')
            ->andWhere('c.submittedAt is null')
            ->getQuery()
            ->getResult()
        ;
    }
    // /**
    //  * @return ApplicationForm[] Returns an array of ApplicationForm objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ApplicationForm
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
