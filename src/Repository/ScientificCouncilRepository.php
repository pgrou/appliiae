<?php

namespace App\Repository;

use App\Entity\ScientificCouncil;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ScientificCouncil|null find($id, $lockMode = null, $lockVersion = null)
 * @method ScientificCouncil|null findOneBy(array $criteria, array $orderBy = null)
 * @method ScientificCouncil[]    findAll()
 * @method ScientificCouncil[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ScientificCouncilRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ScientificCouncil::class);
    }

    // /**
    //  * @return ScientificCouncil[] Returns an array of ScientificCouncil objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ScientificCouncil
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
