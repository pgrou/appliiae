<?php

namespace App\Repository;

use App\Entity\AskLogin;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method AskLogin|null find($id, $lockMode = null, $lockVersion = null)
 * @method AskLogin|null findOneBy(array $criteria, array $orderBy = null)
 * @method AskLogin[]    findAll()
 * @method AskLogin[]    findAllOrderedByStatus()
 * @method AskLogin[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AskLoginRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, AskLogin::class);
    }

    /**
     * @return Person[] Returns an array of Person objects
     */
    public function findAllOrderedByStatus()
    {
        return $this->createQueryBuilder('al')
            ->orderBy('al.status', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

    public function countPending()
    {
        return $this->createQueryBuilder('a')
            ->select('COUNT(a)')
            ->where('a.status is null')
            ->getQuery()
            ->getSingleScalarResult();
        
    }
    // /**
    //  * @return Person[] Returns an array of Person objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Person
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
