<?php

namespace App\Repository;

use App\Entity\AskContact;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method AskContact|null find($id, $lockMode = null, $lockVersion = null)
 * @method AskContact|null findOneBy(array $criteria, array $orderBy = null)
 * @method AskContact[]    findAll()
 * @method AskContact[]    findAllOrderedByCreationAt()
 * @method AskContact[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AskContactRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, AskContact::class);
    }

    /**
     * @return Person[] Returns an array of Person objects
     */
    public function findAllOrderedByCreationAt()
    {
        return $this->createQueryBuilder('al')
            ->orderBy('al.createdAt', 'DESC')
            ->getQuery()
            ->getResult()
        ;
    }

    // /**
    //  * @return Person[] Returns an array of Person objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Person
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
