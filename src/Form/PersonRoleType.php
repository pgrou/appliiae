<?php

namespace App\Form;

use App\Entity\Role;
use App\Entity\Person;
use App\Entity\Nationality;
use App\Repository\RoleRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;


class PersonRoleType extends AbstractType
{
    //QRO - Form used to manage roles for user (Route "person_role" from PersonController)
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        
            ->add(
                'personRoles',
                EntityType::class,
                array(
                    'label'=>'Roles', 
                    'required'=>false,
                    'help'=>'You can select multiple roles', 
                    'class' => Role::class,
                    'choice_label' => 'title',
                    'multiple' => true,
                    'query_builder' => function (RoleRepository $nr) {
                        return $nr->createQueryBuilder('r')
                            ->orderBy('r.title', 'ASC');
                    },
                    'attr'=>array(
                        'class'=>'select2'
                    ),
                    'by_reference' => false,
                )
            )
            
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Person::class,
            'disabled'=>false,
        ]);
    }
}
