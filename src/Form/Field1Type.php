<?php

namespace App\Form;

use App\Entity\Field;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class Field1Type extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'label',
                null,
                array(
                    'label'=>'Label FR'
                )
            )
            ->add(
                'description',
                null,
                array(
                    'label'=>'Description FR'
                )
            )
            ->add(
                'labelEn',
                null,
                array(
                    'label'=>'Label EN'
                )
            )
            ->add(
                'descriptionEn',
                null,
                array(
                    'label'=>'Description EN'
                )
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Field::class,
        ]);
    }
}
