<?php

namespace App\Form;

use App\Entity\Call;
use App\Entity\ScientificCouncil;
use Symfony\Component\Form\AbstractType;
use App\Repository\ScientificCouncilRepository;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class CallType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add(
            'labelFr', 
            null, 
            array(
                'label'=>'Label FR',
                'attr'=>array(
                    'placeholder' => 'Label in french'
                )
            )
        )
        ->add(
            'descriptionFr',
            null, 
            array(
                'label'=>'Description FR',
                'attr'=>array(
                    'placeholder' => 'Description in french'
                )
            )
        )
        ->add(
            'labelEn',
            null,
            array(
                'label'=>'Label EN',
                'attr'=>array(
                    'placeholder'=> 'Label in english'
                )
            )
        )
        ->add(
            'descriptionEn',
            null,
            array(
                'label'=>'Description EN',
                'attr'=>array(
                    'placeholder'=> 'Description in english'
                )
            )
        )
        ->add(
            'openingDate', 
            DateType::class, 
            array(
                'label'=>'Opening date',
                // renders it as a single text box
                'required'=> false,
                'widget' => 'single_text',
                'html5' => false,
                'help' => 'Date format is yyyy-mm-dd',
                'attr'=>array(
                    'class'=> 'datepicker',
                    'placeholder'=>'yyyy-mm-dd',
                )
            )
        )
        ->add(
            'closingDate', 
            DateType::class, 
            array(
                'label'=>'Closing date',
                // renders it as a single text box
                'required'=> false,
                'widget' => 'single_text',
                'html5' => false,
                'help' => 'Date format is yyyy-mm-dd',
                'attr'=>array(
                    'class'=> 'datepicker',
                    'placeholder'=>'yyyy-mm-dd',
                )
            )
        )
            ->add(
                'scientificCouncil',
                EntityType::class, 
                array(
                    // looks for choices from this entity
                    'class' => ScientificCouncil::class,
                    'choice_label' => 'label',
                    'help'=>'Attach this call to an existing Scientific Council',
                    'query_builder' => function (ScientificCouncilRepository $scr) {
                        return $scr->createQueryBuilder('n')
                            ->orderBy('n.id', 'DESC');
                    },
                    'attr'=>array(
                        'class'=>'select2'
                    ),  
                )
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Call::class,
        ]);
    }
}
