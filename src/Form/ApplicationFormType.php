<?php

namespace App\Form;

use App\Entity\Person;
use App\Form\ProjectType;
use App\Entity\ApplicationForm;
use App\Repository\PersonRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ApplicationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'candidate',
                EntityType::class,
                array(
                    'label'=>'candidate', 
                    'required'=>true,
                    'help'=>'Select', 
                    'class' => Person::class,
                    'choice_label' => 'fullname',
                    'disabled'=>$options['disabled'],
                    'placeholder' => 'Select the candidate for this application',
                    'multiple' => false,
                    'mapped'=> false,
                    'query_builder' => function (PersonRepository $pr) {
                        return $pr->createQueryBuilder('p')
                            ->orderBy('p.lastname', 'ASC');
                    },
                    'attr'=>array(
                        'class'=>'select2'
                    ),   
                )
            )
            ->add(
                'project',
                ProjectType::class
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ApplicationForm::class,
        ]);
    }
}
