<?php

namespace App\Form;

use App\Entity\Country;
use App\Entity\PostalAddress;
use App\Repository\CountryRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class PostalAddressType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'label',
                TextareaType::class,
                array(
                    'label'=>'Label',
                    'help'=>'As it should be written on mail',
                    'disabled'=>$options['disabled'],
                    'attr'=>array(
                        'placeholder'=>
                            '3975 Kyle Street
Grand Island, NE 68801 ',
//no indent because html doesn't render "\n" (line break)
                    )
                )
            )
            ->add(
                'country',
                EntityType::class,
                array(
                    'label'=>'Country', 
                    'class' => Country::class,
                    'placeholder'=>'Choose a country',
                    'choice_label' => 'labelEn',
                    'disabled'=>$options['disabled'],
                    'multiple' => false,
                    'query_builder' => function (CountryRepository $cr) {
                        return $cr->createQueryBuilder('n')
                            ->orderBy('n.labelEn', 'ASC');
                    },
                    'attr'=>array(
                        'class'=>'select2 advised'
                    ),   
                )
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PostalAddress::class,
        ]);
    }
}
