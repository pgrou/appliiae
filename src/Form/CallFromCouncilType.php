<?php

namespace App\Form;

use App\Entity\Call;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class CallFromCouncilType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'labelFr', 
                null, 
                array(
                    'attr'=>array(
                        'placeholder' => 'Label in french'
                    )
                )
            )
            ->add(
                'descriptionFr',
                null, 
                array(
                    'attr'=>array(
                        'placeholder' => 'Description in french'
                    )
                )
            )
            ->add(
                'labelEn',
                null,
                array(
                    'attr'=>array(
                        'placeholder'=> 'Label in english'
                    )
                )
            )
            ->add(
                'descriptionEn',
                null,
                array(
                    'attr'=>array(
                        'placeholder'=> 'Description in english'
                    )
                )
            )
            ->add(
                'openingDate', 
                DateType::class, 
                array(
                    'label'=>'Opening date',
                    // renders it as a single text box
                    'required'=> false,
                    'widget' => 'single_text',
                    'html5' => false,
                    'help' => 'Date format is yyyy-mm-dd',
                    'attr'=>array(
                        'class'=> 'datepicker',
                        'placeholder'=>'yyyy-mm-dd',
                    )
                )
            )
            ->add(
                'closingDate', 
                DateType::class, 
                array(
                    'label'=>'Closing date',
                    // renders it as a single text box
                    'required'=> false,
                    'widget' => 'single_text',
                    'html5' => false,
                    'help' => 'Date format is yyyy-mm-dd',
                    'attr'=>array(
                        'class'=> 'datepicker',
                        'placeholder'=>'yyyy-mm-dd',
                    )
                )
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Call::class,
        ]);
    }
}
