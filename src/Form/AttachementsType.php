<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class AttachementsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'formCurriculumVitae',
                FileType::class,
                array(
                    'label'=>'Curriculum vitae (PDF file only, 40Mo max)',
                    'data_class' => null,
                    'required'=>false,
                    'disabled'=>$options['disabled'],
                    'mapped'=>false,
                    'help'=> 'Upload a file overwrite existing one',
                    'attr'=>array(
                    ),
                )
            )
            ->add(
                'formProject',
                FileType::class,
                array(
                    'label'=>'Project (PDF file only, 40Mo max)',
                    'data_class' => null,
                    'disabled'=>$options['disabled'],
                    'required'=>false,
                    'mapped'=>false,
                    'help'=> 'Upload a file overwrite existing one',
                    'attr'=>array(
                    ),
                )
            )
            ->add(
                'formPublication1',
                FileType::class,
                array(
                    'label'=>'Publication 1 (PDF file only, 40Mo max)',
                    'data_class' => null,
                    'disabled'=>$options['disabled'],
                    'required'=>false,
                    'mapped'=>false,
                    'help'=> 'Upload a file overwrite existing one',
                    'attr'=>array(
                    ),
                )
            )
            ->add(
                'formPublication2',
                FileType::class,
                array(
                    'label'=>'Publication 2 (PDF file only, 40Mo max)',
                    'data_class' => null,
                    'disabled'=>$options['disabled'],
                    'required'=>false,
                    'mapped'=>false,
                    'help'=> 'Upload a file overwrite existing one',
                    'attr'=>array(
                    ),
                )
            )
            
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
