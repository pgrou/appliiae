<?php

namespace App\Form;

use App\Entity\AskLogin;
use App\Form\ApplicationType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints\Valid;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextAreaType;

class AskLoginType extends ApplicationType
{
    //QRO - Form used for applying to a call
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add(
            'lastname', 
            TextType::class, 
            array(
                    'label'=>'Lastname',
                    'attr'=>array(
                        'placeholder' => 'Official, as on passport',
                        'disabled'=>$options['disabled'],
                        'class'=>'advised'   
                )
             )
        )
		->add(
			'firstname', 
			TextType::class, 
			array(
				'label'=>'Firstname', 
				'disabled'=> $options['disabled'],
				'attr'=> array(
					'placeholder'=>'Official, as on passport', 
					'class'=>'advised'
				)
			)
		)
		->add(
			'email',
			EmailType::class,
			array(
				'label'=>'Email',
				'help'=>'Provide a valid address, used for login and further exchanges',
				'disabled'=>$options['disabled'],
				'attr'=>array(
					'placeholder'=>'example@domaine.com',
					'class'=> 'advised',
				)
			)
		)
		->add(
			'project',
			TextAreaType::class,
			array(
				'label'=>'Project',
				'help'=>'Summary of the project',
				'disabled'=>$options['disabled'],
				'attr'=>array(
					'placeholder'=>'Summary of the project',
					'class'=> 'advised',
				)
			)
		)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => AskLogin::class,
            'disabled'=>false,
            'constraints' => array(new Valid()),
        ]);
    }
}
