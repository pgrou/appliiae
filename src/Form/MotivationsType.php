<?php

namespace App\Form;

use App\Entity\ApplicationForm;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class MotivationsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'iasKnowledge', 
                TextareaType::class,
                array(
                    'label'=>'How have you heard about IAS-Nantes?',
                    'required'   => false,
                    'disabled'=>$options['disabled'],
                    'attr'=>array(
                        'placeholder'=>' ',
                        'class'=>'advised',
                    )
                ) 
            )
            ->add(
                'previousIasMember',
                TextareaType::class,
                array(
                    'label'=>'Have you previously been a member of an IAS?',
                    'required'   => false,
                    'disabled'=>$options['disabled'],
                    'help'=>'Please specify when, where and the research project',
                    'attr'=>array(
                        'placeholder'=>' ',
                        'class'=>'advised',
                    )
                ) 
            )
            ->add(
                'ongoingApplication',
                TextareaType::class,
                array(
                    'label'=>'Do you have an ongoing application in another IAS?',
                    'help'=>'If yes, where and the research project',
                    'required'   => false,
                    'disabled'=>$options['disabled'],
                    'attr'=>array(
                        'placeholder'=>' ',
                        'class'=>'advised',
                    )
                ) 
            )
            ->add(
                'majorPuplications',
                TextareaType::class,
                array(
                    'label'=>'Major publications (up to 3)',
                    'help'=>'If it\'s an article, please precise where it was published',
                    'required'   => false,
                    'disabled'=>$options['disabled'],
                    'attr'=>array(
                        'placeholder'=>' ',
                        'class'=>'advised',
                    )
                ) 
            )
            ->add(
                'motivations',
                TextareaType::class,
                array(
                    'label'=>'What motivates you to apply for a research fellowship at the IAS-Nantes?',
                    'required'   => false,
                    'disabled'=>$options['disabled'],
                    'attr'=>array(
                        'placeholder'=>' ',
                        'class'=>'advised',
                    )
                ) 
            )
            ->add(
                'benefits',
                TextareaType::class,
                array(
                    'label'=>'How do you think you might benefit from the exchanges with other Fellows?',
                    'required'   => false,
                    'disabled'=>$options['disabled'],
                    'attr'=>array(
                        'placeholder'=>' ',
                        'class'=>'advised',
                    )
                ) 
            )
            ->add(
                'biography',
                TextareaType::class,
                array(
                    'label'=>'Biography',
                    'help'=>'Approximately 10 lines',
                    'required'   => false,
                    'disabled'=>$options['disabled'],
                    'attr'=>array(
                        'placeholder'=>' ',
                        'class'=>'advised',
                    )
                ) 
            )

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ApplicationForm::class,
        ]);
    }
}
