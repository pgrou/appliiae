<?php

namespace App\Form;

use App\Entity\Field;
use App\Entity\Project;
use App\Form\ApplicationType;
use App\Repository\FieldRepository;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class ProjectType extends ApplicationType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'title', 
                TextType::class, 
                array(
                    'label'=>'Title', 
                    'attr'=>array(
                        'placeholder'=>'Research project title',
                        'disabled'=>$options['disabled'],
                        'class'=>'advised',
                    )
                )
            )
            ->add(
                'summary', 
                TextareaType::class, 
                array(
                    'label'=>'Summary',
                    'disabled'=>$options['disabled'],
                    'attr'=>array(
                        'placeholder'=>'Summary of research project',
                        'class'=>'advised',
                        'rows'=>'7',
                    )
                )
            )
            ->add(  
                'mainSubjects', 
                TextareaType::class, 
                array(
                    'label'=>'Main subjects', 
                    'required'   => false,
                    'disabled'=>$options['disabled'],
                    'attr'=>array(
                        'placeholder'=>'What main subjects are your works inspired from ?', 
                        'class'=>'advised',
                        'rows'=>'7',
                    )
                )
            )
            ->add(
                'previousPublicationsEnlightning', 
                TextareaType::class, 
                array(
                    'label'=>'Previous publications',
                    'required'   => false,
                    'attr'=>array(
                        'placeholder'=>'In what ways do your previous publications enlighten those subjects ?',
                        'disabled'=>$options['disabled'],
                        'class'=>'advised',
                        'rows'=>'7',
                    )
                )
            )
            ->add(
                'conciliatePreviousWorks', 
                TextareaType::class, 
                array(
                    'label'=>'Links with previous works',
                    'required'   => false,
                    'attr'=>array(
                        'placeholder'=>'In what ways does your research project for the IAS-Nantes conciliate with your previous works on those subjects?',
                        'disabled'=>$options['disabled'],
                        'class'=>'advised',
                        'rows'=>'7',
                    )
                )
            )
            ->add('mainField',
                EntityType::class, 
                array(
                    'label'=>'Main discipline', 
                    'help'=>'Chose a main discipline for this project', 
                    'class' => Field::class,
                    'choice_label' => 'labelEn',
                    'disabled'=>$options['disabled'],
                    'placeholder' => 'Choose an option',
                    'attr'=>array(
                    ),
                    'query_builder' => function (FieldRepository $fr) {
                        return $fr->createQueryBuilder('f')
                            ->orderBy('f.labelEn', 'ASC');
                    },
                    
                )
            )
            ->add('specialty',
                TextType::class, 
                array(
                    'label'=>'Specialty', 
                    'required'   => false,
                    'disabled'=>$options['disabled'],
                    'attr'=>array(
                    )
                )
            )
            
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Project::class,
            'disabled' => false,
        ]);
    }
}
