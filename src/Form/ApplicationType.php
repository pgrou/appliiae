<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ApplicationType extends AbstractType
{
    /**
     * Undocumented function
     *
     * @param string $label
     * @param string $placeholder
     * @param string $tooltip 
     * @param array $options
     * @return void
     */
    protected function getConfiguration($label, $placeholder, $disabled=false,$tooltip='', $options = []){
        return array_merge([
            'label'=>$label,
            'attr'=> [
                'placeholder' => $placeholder,
                'data-toggle'=>'tooltip',
                'title'=>$tooltip,
                'disabled'=>$disabled,
            ]
            ],$options);
    }
}
