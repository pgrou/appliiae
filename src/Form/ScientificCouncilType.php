<?php

namespace App\Form;

use App\Entity\ScientificCouncil;
use App\Form\CallFromCouncilType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class ScientificCouncilType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('label')
            ->add('description')
            ->add(
                'date', 
                DateType::class, 
                array(
                    'label'=>'Date',
                    // renders it as a single text box
                    'required'=> false,
                    'widget' => 'single_text',
                    'html5' => false,
                    'help' => 'Date format is yyyy-mm-dd',
                    'attr'=>array(
                        'class'=> 'datepicker',
                        'placeholder'=>'yyyy-mm-dd',
                    )
                )
            )
            ->add(
                'calls',
                CollectionType::class,
                array(
                    'entry_type' => CallFromCouncilType::class,
                    'allow_add' => true,
                )
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ScientificCouncil::class,
        ]);
    }
}
