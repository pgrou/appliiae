<?php

namespace App\Form;

use App\Entity\ApplicationForm;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\RangeType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class AdministrativeType extends AbstractType
{
  
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'currentEmployer', 
                TextType::class, 
                array(
                    'label'=>'Current employer',
                    'required'=> false,
                    'disabled'=>$options['disabled'],
                    'attr'=>array(
                        'placeholder'=>' ',
                        'class'=>'advised',
                    )
                )
            )
            ->add(
                'positionTitle',
                TextType::class, 
                array(
                    'label'=>'Current professional position and academic title(s)',
                    'required'=> false,
                    'attr'=>array(
                        'placeholder'=>' ',
                        'disabled'=>$options['disabled'],
                        'class'=>'advised',
                    )
                )
            )
            ->add(
                'positionSince',
                DateType::class, 
                array(
                    'label'=>'position occupied since',
                    'required'=> false,
                    'widget' => 'single_text',
                    'html5' => false,
                    'help' => 'Date format is yyyy-mm-dd',
                    'attr'=>array(
                        'disabled'=>$options['disabled'],
                        'class'=> 'advised datepicker',
                        'placeholder'=>'yyyy-mm-dd',
                    )
                )
            )
            ->add(
                'previousPositions',
                TextareaType::class, 
                array(
                    'label'=>'previous positions',
                    'required'=> false,
                    'disabled'=>$options['disabled'],
                    'help'=>'Up to 3',
                    'attr'=>array(
                        'placeholder'=>' ',
                        'class'=>'advised',
                    )
                )
            )
            ->add(
                'choice1StartMonth',
                ChoiceType::class, 
                array(
                    'label'=>'First choice starting month requested',
                    'required'=> false,
                    'disabled'=>$options['disabled'],
                    'help'=> 'The accademic years runs from October to june',
                    'choices'  => array(
                        'October'   => 10,
                        'January'   => 1,
                        'April'     => 4,
                       
                    ),
                    'attr'=>array(
                        'placeholder'=>' ',
                        'class'=>'advised',
                        'onChange'=>'restrictMonth(this)',
                    )
                )
            )
            ->add(
                'choice1NumberMonths',
                ChoiceType::class, 
                array(
                    'label'=>'First choice number of months requested',
                    'required'=> false,
                    'disabled'=>$options['disabled'],
                    'choices'  => array(
                        '3'   => 3,
                        '6'   => 6,
                        '9'     => 9,
                    ),
                    'placeholder'=>'3 to 9',
                    'attr'=>array(
                        'class'=>'advised',
                        
                    )
                )
            )
            ->add(
                'choice2StartMonth',
                ChoiceType::class, 
                array(
                    'label'=>'Second choice starting month requested',
                    'required'=> false,
                    'disabled'=>$options['disabled'],
                    'help'=> 'The accademic years runs from October to june',
                    'placeholder'=>' ',
                    'choices'  => array(
                        'October'   => 10,
                        'January'   => 1,
                        'April'     => 4,
                    ),
                    'attr'=>array(
                        'class'=>'advised',
                        'onChange'=>'restrictMonth(this)',

                    )
                )
            )
            ->add(
                'choice2NumberMonths',
                ChoiceType::class, 
                array(
                    'label'=>'Second choice number of months requested',
                    'required'=> false,
                    'disabled'=>$options['disabled'],
                    'choices'  => array(
                        '3'   => 3,
                        '6'   => 6,
                        '9'     => 9,
                    ),
                    'placeholder'=>'3 to 9',
                    'attr'=>array(
                        'class'=>'advised',
                    )
                )
            )
            ->add(
                'sabbatical',
                TextType::class,
                array(
                    'help'=> 'Are you likely to get a sabbatical for the fellowship period ?',
                    'disabled'=>$options['disabled'],
                    'required'=> false,
                    'attr'=>array(
                        'placeholder'=>' ',
                        'class'=>'advised',
                    )
                )
            )
            ->add(
                'financialSupport', 
                TextareaType::class, 
                array(
                    'label'=>'Financial support',
                    'help'=>'In case your application was accepted by IAS-Nantes, could you get a financial support from your university or another institution (sabbatical leave, grant...) ? If yes, please specify the modalities.',
                    'disabled'=>$options['disabled'],
                    'required'=> false,
                    'attr'=>array(
                        'placeholder'=>' ',
                        'class'=>'advised',
                    )
                )
            )
            ->add(
                'usualSalary', 
                TextType::class,
                array(
                    'label'=>'Usual monthy salary',
                    'help'=> 'Precise amount and currency',
                    'disabled'=>$options['disabled'],
                    'required'=> false,
                    'attr'=>array(
                        'placeholder'=>' ',
                        'class'=>'advised',
                        'data-toggle'=>'tooltip',
                        'data-placement'=>'bottom',
                        'title'=>'When applicant cannot get his/her origin institution part or the totality of his/her income during the period of the felloship, IAS-Nantes pays a residency allowance aiming to compensate, withing a certain limit. In order to do so the IAS refers to the usual monthly salary',
                    )
                )
            )
            ->add(
                'accompanying',
                TextareaType::class,
                array(
                    'label'=>'Should your application be accepted, would you come alone / with your partner / with one or more children?',
                    'disabled'=>$options['disabled'],
                    'required'=> false,
                    'help'=>'Precise age of the children',
                    'attr'=>array(
                        'placeholder'=>' ',
                        'class'=>'advised',
                    )
                )
            )
            ->add(
                'resideInNantes',
                CheckboxType::class, 
                array(
                    'label'=>'Stay at Nantes',
                    'required'=> false,
                    'disabled'=>$options['disabled'],
                    'help'=> 'Would you be able to stay at Nantes during fellowship?',
                    'attr'=>array(
                        'class'=>'advised',
                    )
                )
            )
            
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ApplicationForm::class,
        ]);
    }
}
