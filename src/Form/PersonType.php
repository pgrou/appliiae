<?php

namespace App\Form;

use App\Entity\Person;
use App\Entity\Nationality;
use App\Form\ApplicationType;
use App\Form\PostalAddressType;
use App\Repository\NationalityRepository;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints\Valid;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;

class PersonType extends ApplicationType
{
    //QRO - Form used for applying to a call
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add(
            'title', 
            TextType::class, 
            array(
                    'required'=>false,
                    'label'=>'Title',
                    'attr'=>array(
                        'placeholder' => 'Pr, Dr ...',
                        'disabled'=>$options['disabled'],
                    )
                )
            )
        ->add(
            'lastname', 
            TextType::class, 
            array(
                    'label'=>'Lastname',
                    'attr'=>array(
                        'placeholder' => 'Official, as on passport',
                        'disabled'=>$options['disabled'],
                        'class'=>'advised'   
                    )
                )
            )
            ->add(
                'firstname', 
                TextType::class, 
                array(
                    'label'=>'Firstname', 
                    'disabled'=> $options['disabled'],
                    'attr'=> array(
                        'placeholder'=>'Official, as on passport', 
                        'class'=>'advised'
                    )
                )
            )
            ->add(
                'usualLastname', 
                TextType::class,
                array(
                    'label'=>'Common lastname',
                    'required' => false,
                    'disabled'=>$options['disabled'],
                    'attr'=>array(
                        'placeholder'=>'Only if different from passport',
                    )
                )
            )
            ->add(
                'usualFirstname', 
                TextType::class,
                array(
                    'label'=>'Common firstname',
                    'required' => false,
                    'disabled'=>$options['disabled'],
                    'attr'=>array(
                        'placeholder'=>'Only if different from passport',
                    )
                )
            )
            
            ->add(
                'gender',
                ChoiceType::class, 
                array(
                    'placeholder' => 'Choose an option',
                    'label'=>'Gender',
                    'required'=> false,
                    'choices'  => array(
                        'Male' => 1,
                        'Female' => 2,
                        'Neutral' => 3,
                    ),
                    'attr'=>array(
                        'disabled'=>$options['disabled'],
                        'class'=> 'advised',
                    )
                )
            )
            ->add(
                'dateOfBirth', 
                DateType::class, 
                array(
                    'label'=>'Date of birth',
                    // renders it as a single text box
                    'required'=> false,
                    'widget' => 'single_text',
                    'html5' => false,
                    'help' => 'Date format is yyyy-mm-dd',
                    'attr'=>array(
                        'disabled'=>$options['disabled'],
                        'class'=> 'advised datepicker',
                        'placeholder'=>'yyyy-mm-dd',
                    )
                )
            )
            ->add(
                'placeOfBirth',
                TextType::class,
                array(
                    'label'=> 'Place of birth',
                    'required'=> false,
                    'help'=> 'City / country',
                    'disabled'=>$options['disabled'],
                    'attr'=>array(
                    )
                )
            )
            ->add(
                'nationalities',
                EntityType::class,
                array(
                    'label'=>'Nationality', 
                    'required'=>false,
                    'help'=>'You can select multiple nationalities', 
                    'class' => Nationality::class,
                    'choice_label' => 'labelEn',
                    'disabled'=>$options['disabled'],
                    'placeholder' => 'Select your nationality',
                    'multiple' => true,
                    'query_builder' => function (NationalityRepository $nr) {
                        return $nr->createQueryBuilder('n')
                            ->orderBy('n.labelEn', 'ASC');
                    },
                    'attr'=>array(
                        'class'=>'select2'
                    ),   
                )
            )
            ->add(
                'email',
                EmailType::class,
                array(
                    'label'=>'Email',
                    'help'=>'Provide a valid address, used for further exchanges',
                    'disabled'=>$options['disabled'],
                    'attr'=>array(
                        'placeholder'=>'example@domaine.com',
                        'class'=> 'advised',
                    )
                )
            )
            ->add(
                'phoneNumber',
                TelType::class,
                array(
                    'label'=>'Phone number',
                    'required'=> false,
                    'help'=>'Provide a valid phone number, used for further exchanges',
                    'disabled'=>$options['disabled'],
                    'attr'=>array(
                        'placeholder'=>'+33 612345678',
                        'class'=> 'advised',
                    )
                )
            )
            ->add(
                'postalAddress',
                PostalAddressType::class

                
            )
            ->add(
                'webPage',
                TextType::class,
                array(
                    'label'=>'Personal webpage',
                    'required'=> false,
                    'disabled'=>$options['disabled'],
                    'attr'=>array(
                        'placeholder'=>'https://address_for_my_personal_page.com/'
                    )
                )
            )
            ->add(
                'phDYear',
                NumberType::class,
                array(
                    'label'=>'Year of PhD',
                    'required'=> false,
                    'disabled'=>$options['disabled'],
                    'attr'=>array(
                        'class'=> 'advised',
                        'size'=>4,
                        'maxlength'=>4,
                        'placeholder'=>' ',
                    )
                )
            )
            ->add(
                'phDUniversity',
                TextType::class,
                array(
                    'label'=>'University of PhD',
                    'required'=> false,
                    'help'=>'Name, City, Country',
                    'disabled'=>$options['disabled'],
                    'attr'=>array(
                        'class'=> 'advised',
                        'placeholder'=>' ',
                    )
                )
            )
            ->add(
                'englishLevel',
                ChoiceType::class, 
                array(
                    'placeholder' => 'Choose an option',
                    'label'=>'English level',
                    'required'=> false,
                    'choices'  => array(
                        'Beginner' => 'B',
                        'Intermediate' => 'I',
                        'Advanced or mother tongue' => 'A',
                    ),
                    'attr'=>array(
                        'disabled'=>$options['disabled'],
                        'class'=> 'advised select2',
                    )
                )
            )
            ->add(
                'frenchLevel',
                ChoiceType::class, 
                array(
                    'placeholder' => 'Choose an option',
                    'label'=>'French level',
                    'required'=> false,
                    'choices'  => array(
                        'Beginner' => 'B',
                        'Intermediate' => 'I',
                        'Advanced or mother tongue' => 'A',
                    ),
                    'attr'=>array(
                        'disabled'=>$options['disabled'],
                        'class'=> 'advised select2',
                    )
                )
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Person::class,
            'disabled'=>false,
            'constraints' => array(new Valid()),
        ]);
    }
}
