<?php

namespace App\Form;

use App\Entity\AskLogin;
use App\Entity\Person;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Valid;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class RegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email')
            ->add('firstname')
            ->add('lastname')
            ->add('password', PasswordType::class)
            ->add('confirmPassword', PasswordType::class)
            ->add('project', TextareaType::class)
            ->add('status')
            ->add('processedAt')
            ->add('createdAt')
            ->add('grpdConsentAt', CheckboxType::class, array(
                'mapped' => false,
                'label'    => 'Do you consent to the GRPD terms ?',
                'required' => true,
            ));
            
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => AskLogin::class,
            
        ]);
    }
}
