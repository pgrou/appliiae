<?php

namespace App\DataFixtures;

use App\Entity\Project;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class ProjectFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        for($i=0; $i<5; $i++)
        {
            $project = new Project();

            $project->setTitle("Title".$i);
            $project->setSummary("Summary".$i);
            $project->setMainField($this->getReference("Philosophie"));
            $project->setPerson($this->getReference("person".$i));

            $this->addReference("project".$i, $project);

            $manager->persist($project);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            PersonFixtures::class,
            FieldFixtures::class
        );
    }
}
