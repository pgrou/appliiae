<?php

namespace App\DataFixtures;

use App\Entity\Country;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class CountryFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        
        $country = new Country();
        $country->setAlphaCode2("FR");
        $country->setAlphaCode3("FRA");
        $country->setLabelFr("France");
        $country->setLabelEn("France");
        $country->setRegion($this->getReference("Europe"));
        $this->addReference("FR", $country);
        $manager->persist($country);
        
        $country = new Country();
        $country->setAlphaCode2("GB");
        $country->setAlphaCode3("GBR");
        $country->setLabelFr("Royaume-Uni");
        $country->setLabelEn("United Kingdom");
        $country->setRegion($this->getReference("Europe"));
        $this->addReference("GB", $country);
        $manager->persist($country);

        $country = new Country();
        $country->setAlphaCode2("CN");
        $country->setAlphaCode3("CHN");
        $country->setLabelFr("Chine");
        $country->setLabelEn("China");
        $country->setRegion($this->getReference("Asie"));
        $this->addReference("CN", $country);
        $manager->persist($country);
        
        $country = new Country();
        $country->setAlphaCode2("EG");
        $country->setAlphaCode3("EGY");
        $country->setLabelFr("Egypte");
        $country->setLabelEn("Egypt");
        $country->setRegion($this->getReference("Afrique"));
        $this->addReference("EG", $country);
        $manager->persist($country);

        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            RegionFixtures::class,
        );
    }
}
