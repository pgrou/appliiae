<?php

namespace App\DataFixtures;

use App\Entity\ApplicationForm;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class ApplicationFormFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        for($i=0; $i<5; $i++)
        {
            $application = new ApplicationForm();

            $application->setCall($this->getReference("call".$i%3));
            $application->setProject($this->getReference("project".$i));


            $this->addReference("application".$i, $application);

            $manager->persist($application);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            CallFixtures::class,
            ProjectFixtures::class
        );
    }
}
