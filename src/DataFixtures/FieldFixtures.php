<?php

namespace App\DataFixtures;

use App\Entity\Field;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class FieldFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $fields = array(
            array(
                "label" => "Maths",
                "labelEn" => "Maths"
            ),
            array(
                "label" => "Physique",
                "labelEn" => "Physics"
            ),
            array(
                "label" => "Philosophie",
                "labelEn" => "Philosophy"
            )
        );

        foreach($fields as $temp)
        {
            $field = new Field();
            $field->setLabel($temp["label"]);
            $field->setLabelEn($temp["labelEn"]);
            $this->addReference($temp["label"], $field);
            
            $manager->persist($field);
        }

        $manager->flush();
    }
}
