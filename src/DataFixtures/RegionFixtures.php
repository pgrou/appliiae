<?php

namespace App\DataFixtures;

use App\Entity\Region;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class RegionFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $region = new Region();
        $region->setLabelFr("Europe");
        $region->setLabelEn("Europe");
        $manager->persist($region);
        $this->addReference("Europe", $region);


        $region = new Region();
        $region->setLabelFr("Asie");
        $region->setLabelEn("Asia");
        $manager->persist($region);
        $this->addReference("Asie", $region);
        
        $region = new Region();
        $region->setLabelFr("Afrique");
        $region->setLabelEn("Africa");
        $manager->persist($region);
        $this->addReference("Afrique", $region);

        $manager->flush();
    }
}
