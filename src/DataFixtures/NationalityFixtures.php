<?php

namespace App\DataFixtures;

use App\Entity\Nationality;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class NationalityFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $nationality = new Nationality();
        $nationality->setLabelFr("Francais");
        $nationality->setLabelEn("French");
        $nationality->setCountry($this->getReference("FR"));
        $this->addReference("NatFR", $nationality);
        $manager->persist($nationality);

        $nationality = new Nationality();
        $nationality->setLabelFr("Anglais");
        $nationality->setLabelEn("English");
        $nationality->setCountry($this->getReference("GB"));
        $this->addReference("NatGB", $nationality);
        $manager->persist($nationality);
        
        $nationality = new Nationality();
        $nationality->setLabelFr("Chinois");
        $nationality->setLabelEn("Chinese");
        $nationality->setCountry($this->getReference("CN"));
        $this->addReference("NatCN", $nationality);
        $manager->persist($nationality);
        
        $nationality = new Nationality();
        $nationality->setLabelFr("Egyptien");
        $nationality->setLabelEn("Egyptian");
        $nationality->setCountry($this->getReference("EG"));
        $this->addReference("NatEG", $nationality);
        $manager->persist($nationality);

        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            CountryFixtures::class,
        );
    }
}
