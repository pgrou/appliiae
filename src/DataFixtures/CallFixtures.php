<?php

namespace App\DataFixtures;

use App\Entity\Call;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Symfony\Component\Validator\Constraints\DateTime;

class CallFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        for($i=0; $i<3; $i++)
        {
            $call = new Call();
            
            $call->setLabelFr("Call numéro ".$i);
            $call->setLabelEn("Call number ".$i);
            $call->setOpeningDate(new \DateTime("2019-03-01"));
            $call->setClosingDate(new \DateTime("2019-04-01"));
            $call->setScientificCouncil($this->getReference("council".$i));

            $this->addReference("call".$i, $call);

            $manager->persist($call);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            ScientificCouncilFixtures::class
        );
    }
}
