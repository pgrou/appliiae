<?php

namespace App\DataFixtures;

use App\Entity\Role;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class RoleFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $role = new Role();
        $role->setTitle("ROLE_ADMIN");
        $this->addReference("Admin", $role);
        $manager->persist($role);

        $manager->flush();
    }
}
