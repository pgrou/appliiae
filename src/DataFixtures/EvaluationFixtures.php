<?php

namespace App\DataFixtures;

use App\Entity\Evaluation;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class EvaluationFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        for($i=0; $i<5; $i++)
        {
            $evaluation = new Evaluation();

            $evaluation->setApplicationForm($this->getReference("application".$i));
            $evaluation->setExpert($this->getReference("expert".$i));

            $this->addReference("evaluation".$i, $evaluation);

            $manager->persist($evaluation);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            PersonFixtures::class,
            ApplicationFormFixtures::class
        );
    }
}
