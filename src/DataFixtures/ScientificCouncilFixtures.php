<?php

namespace App\DataFixtures;

use App\Entity\ScientificCouncil;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class ScientificCouncilFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        for($i=0; $i<4; $i++)
        {
            $council = new ScientificCouncil();

            $council->setLabel("council".$i);

            $council->addPerson($this->getReference("person".$i));
            $council->addPerson($this->getReference("person".$i*2));

            $this->addReference("council".$i, $council);

            $manager->persist($council);
        }

        

        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            PersonFixtures::class
        );
    }
}
