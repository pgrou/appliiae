<?php

namespace App\DataFixtures;

use App\Entity\Person;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class PersonFixtures extends Fixture implements DependentFixtureInterface
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $persons = array(
            array(
                "field" => "Maths"
            ),
            array(
                "field" => "Maths"
            ),
            array(
                "field" => "Physique"
            ),
            array(
                "field" => "Physique"
            ),
            array(
                "field" => "Physique"
            ),
            array(
                "field" => "Philosophie"
            ),
            array(
                "field" => "Philosophie"
            )
        );

        foreach($persons as $i => $temp){
            $person = new Person();

            $person->setLastname("Person".$i);
            $person->setEmail($i."@mail.com");
            $person->setPassword($this->encoder->encodePassword($person, "userpass"));
            $person->addField($this->getReference($temp["field"]));

            $this->addReference("person".$i, $person);
            $manager->persist($person);
        }

        foreach($persons as $i => $temp){
            $person = new Person();

            $person->setLastname("Expert".$i);
            $person->setEmail("expert".$i."@mail.com");
            $person->setPassword($this->encoder->encodePassword($person, "userpass"));
            $person->addField($this->getReference($temp["field"]));
            
            $this->addReference("expert".$i, $person);
            $manager->persist($person);
        }

        $person = new Person();

        $person->setLastname("Admin");
        $person->setEmail("admin@mail.com");
        $person->setPassword($this->encoder->encodePassword($person, "adminpass"));
        $person->addPersonRole($this->getReference("Admin"));

        $manager->persist($person);

        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            RoleFixtures::class,
            NationalityFixtures::class,
            FieldFixtures::class
        );
    }
}
