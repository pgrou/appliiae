<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CountryRepository")
 */
class Country
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=2)
     */
    private $alphaCode2;

    /**
     * @ORM\Column(type="string", length=3, nullable=true)
     */
    private $alphaCode3;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $labelFr;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $labelEn;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Region", inversedBy="countries")
     */
    private $region;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Nationality", mappedBy="country")
     */
    private $nationalities;

    public function __construct()
    {
        $this->nationalities = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAlphaCode2(): ?string
    {
        return $this->alphaCode2;
    }

    public function setAlphaCode2(string $alphaCode2): self
    {
        $this->alphaCode2 = $alphaCode2;

        return $this;
    }

    public function getAlphaCode3(): ?string
    {
        return $this->alphaCode3;
    }

    public function setAlphaCode3(?string $alphaCode3): self
    {
        $this->alphaCode3 = $alphaCode3;

        return $this;
    }

    public function getLabelFr(): ?string
    {
        return $this->labelFr;
    }

    public function setLabelFr(?string $labelFr): self
    {
        $this->labelFr = $labelFr;

        return $this;
    }

    public function getLabelEn(): ?string
    {
        return $this->labelEn;
    }

    public function setLabelEn(?string $labelEn): self
    {
        $this->labelEn = $labelEn;

        return $this;
    }

    public function getRegion(): ?Region
    {
        return $this->region;
    }

    public function setRegion(?Region $region): self
    {
        $this->region = $region;

        return $this;
    }

    /**
     * @return Collection|Nationality[]
     */
    public function getNationalities(): Collection
    {
        return $this->nationalities;
    }

    public function addNationality(Nationality $nationality): self
    {
        if (!$this->nationalities->contains($nationality)) {
            $this->nationalities[] = $nationality;
            $nationality->setCountry($this);
        }

        return $this;
    }

    public function removeNationality(Nationality $nationality): self
    {
        if ($this->nationalities->contains($nationality)) {
            $this->nationalities->removeElement($nationality);
            // set the owning side to null (unless already changed)
            if ($nationality->getCountry() === $this) {
                $nationality->setCountry(null);
            }
        }

        return $this;
    }
}
