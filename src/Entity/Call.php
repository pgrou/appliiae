<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CallRepository")
 * @ORM\Table(name="call_app")
 */
class Call
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(max="255")
     */
    private $labelFr;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $descriptionFr;
    
    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(max="255")
     */
    private $labelEn;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $descriptionEn;
    /**
     * @ORM\Column(type="datetime")
     */
    private $openingDate;

    /**
     * @ORM\Column(type="datetime")
     */
    private $closingDate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ScientificCouncil", inversedBy="calls")
     * @ORM\JoinColumn(nullable=false)
     */
    private $scientificCouncil;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ApplicationForm", mappedBy="call")
     */
    private $applicationForms;

    public function __construct()
    {
        $this->applicationForms = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabelFr(): ?string
    {
        return $this->labelFr;
    }

    public function setLabelFr(string $labelFr): self
    {
        $this->labelFr = $labelFr;

        return $this;
    }

    public function getDescriptionFr(): ?string
    {
        return $this->descriptionFr;
    }

    public function setDescriptionFR(?string $descriptionFr): self
    {
        $this->descriptionFr = $descriptionFr;

        return $this;
    }
    public function getLabelEn(): ?string
    {
        return $this->labelEn;
    }

    public function setLabelEn(string $labelEn): self
    {
        $this->labelEn = $labelEn;

        return $this;
    }

    public function getDescriptionEn(): ?string
    {
        return $this->descriptionEn;
    }

    public function setDescriptionEn(?string $descriptionEn): self
    {
        $this->descriptionEn = $descriptionEn;

        return $this;
    }
    public function getOpeningDate(): ?\DateTimeInterface
    {
        return $this->openingDate;
    }

    public function setOpeningDate(\DateTimeInterface $openingDate): self
    {
        $this->openingDate = $openingDate;

        return $this;
    }

    public function getClosingDate(): ?\DateTimeInterface
    {
        return $this->closingDate;
    }

    public function setClosingDate(\DateTimeInterface $closingDate): self
    {
        $this->closingDate = $closingDate;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getScientificCouncil(): ?ScientificCouncil
    {
        return $this->scientificCouncil;
    }

    public function setScientificCouncil(?ScientificCouncil $scientificCouncil): self
    {
        $this->scientificCouncil = $scientificCouncil;

        return $this;
    }
    
    public function getIsOpen()
    {
        $today = new \DateTime;
        if($this->openingDate < $today && $this->closingDate > $today){
            return true;
        }else{
            return false;
        }
        
    }


    /**
     * @return Collection|ApplicationForm[]
     */
    public function getApplicationForms(): Collection
    {
        return $this->applicationForms;
    }

    public function addApplicationForm(ApplicationForm $applicationForm): self
    {
        if (!$this->applicationForms->contains($applicationForm)) {
            $this->applicationForms[] = $applicationForm;
            $applicationForm->setCall($this);
        }

        return $this;
    }

    public function removeApplicationForm(ApplicationForm $applicationForm): self
    {
        if ($this->applicationForms->contains($applicationForm)) {
            $this->applicationForms->removeElement($applicationForm);
            // set the owning side to null (unless already changed)
            if ($applicationForm->getCall() === $this) {
                $applicationForm->setCall(null);
            }
        }

        return $this;
    }
    /**
     * Return application if person given in parameter is a candidate. Return null othewise
     * @param Person $person
     * @return ApplicationForm
     */
    public function isCandidate(Person $person)
    {
        foreach ($this->applicationForms as $application) {
            if($application->getProject()->getPerson() === $person){
                return $application;
            }
        }
        return null;
    }

    public function getSubmittedApplications(){
   
        $submittedApplicationForms = $this->applicationForms->filter(function ($item) {
            return ($item->getSubmittedAt() !== null);
        });
        return $submittedApplicationForms;
    }
    public function getUnsubmittedApplications(){
   
        $unsubmittedApplicationForms = $this->applicationForms->filter(function ($item) {
            return ($item->getSubmittedAt() === null);
        });
        return $unsubmittedApplicationForms;
    }
}
