<?php

namespace App\Entity;

use Cocur\Slugify\Slugify;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface ;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass="App\Repository\PersonRepository")
 * @UniqueEntity(
 * fields={"email"},
 * message="This email is already exists."
 * )
 */
class Person implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotNull(message="Lastname field cannot be empty")
     * @Assert\Length(max="255")
     */
    private $lastname;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(max="255")
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(max="255")
     */
    private $usualLastname;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(max="255")
     */
    private $usualFirstname;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $gender;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * 
     */
    private $dateOfBirth;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(max="255")
     */
    private $placeOfBirth;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Evaluation", mappedBy="expert")
     */
    private $evaluations;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(max="255")
     * @Assert\Email()
     * @Assert\NotBlank
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(
     *              min="8", 
     *              minMessage="At least 8 char",
     *              max="255"
     *          )
     */
    private $password;


    /**
     * @Assert\EqualTo(propertyPath="password", message="Passwords must be the same")
     */
    private $confirmPassword;

    /**
     * 
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $verifiedEmailAt;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Project", mappedBy="person", orphanRemoval=true)
     */
    private $projects;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Role", mappedBy="persons")
     */
    private $personRoles;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $grpdConsentAt;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(max="255")
     * @Assert\Url
     */
    private $webPage;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(max="4")
     */
    private $phDYear;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(max="255")
     */
    private $phDUniversity;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(max="255")
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(max="255")
     */
    private $resetPasswordToken;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $tokenExpireAt;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Nationality", inversedBy="people")
     */
    private $nationalities;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(max="255")
     */
    private $phoneNumber;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\PostalAddress", mappedBy="person", cascade={"persist", "remove"})
     */
    private $postalAddress;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $frenchLevel;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $englishLevel;
    
    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $expert;
    
    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $expert_start;
    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $expert_end;
    
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $expert_reasonend;
    
    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Field", inversedBy="people")
     */
    private $fields;
    
    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\ScientificCouncil", inversedBy="members")
     */
    private $scientificCouncils;


    public function __construct()
    {
        $this->evaluations = new ArrayCollection();
        $this->projects = new ArrayCollection();
        $this->personRoles = new ArrayCollection();
        $this->nationalities = new ArrayCollection();
        $this->fields = new ArrayCollection();
        $this->scientificCouncils = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = strtoupper($lastname);

        return $this;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(?string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getUsualLastname(): ?string
    {
        return $this->usualLastname;
    }

    public function setUsualLastname(?string $usualLastname): self
    {
        $this->usualLastname = $usualLastname;

        return $this;
    }

    public function getUsualFirstname(): ?string
    {
        return $this->usualFirstname;
    }

    public function setUsualFirstname(?string $usualFirstname): self
    {
        $this->usualFirstname = $usualFirstname;

        return $this;
    }

    public function getGender(): ?int
    {
        return $this->gender;
    }

    public function setGender(?int $gender): self
    {
        $this->gender = $gender;
        return $this;
    }

    public function getDateOfBirth(): ?\DateTimeInterface
    {
        return $this->dateOfBirth;
    }

    public function setDateOfBirth(?\DateTimeInterface $dateOfBirth): self
    {
        $this->dateOfBirth = $dateOfBirth;
        return $this;
    }

    public function getPlaceOfBirth(): ?string
    {
        return $this->placeOfBirth;
    }
    public function setPlaceOfBirth(?string $placeOfBirth): self
    {
        $this->placeOfBirth = $placeOfBirth;

        return $this;
    }

    /**
     * @return Collection|Evaluation[]
     */
    public function getEvaluations(): Collection
    {
        return $this->evaluations;
    }

    public function addEvaluation(Evaluation $evaluation): self
    {
        if (!$this->evaluations->contains($evaluation)) {
            $this->evaluations[] = $evaluation;
            $evaluation->setExpert($this);
        }
        return $this;
    }

    public function removeEvaluation(Evaluation $evaluation): self
    {
        if ($this->evaluations->contains($evaluation)) {
            $this->evaluations->removeElement($evaluation);
            // set the owning side to null (unless already changed)
            if ($evaluation->getExpert() === $this) {
                $evaluation->setExpert(null);
            }
        }
        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getConfirmPassword()
    {
        return $this->confirmPassword;
    }
    
    public function setConfirmPassword(string $confirmPassword): self
    {
        $this->confirmPassword = $confirmPassword;
        return $this;
    }
    
    public function getUsername(){
        return $this->email;
    }

    public function eraseCredentials(){}
        
    public function getSalt(){}

    public function getRoles(){
        $roles = $this->personRoles->map(function($role){
            return $role->getTitle();
        })->toArray();
        $roles[] = 'ROLE_USER';
        return $roles;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }
    /**
     * Used to automatically persist createdAt value
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        $this->createdAt = new \DateTime();
    }

    public function getVerifiedEmailAt(): ?\DateTimeInterface
    {
        return $this->verifiedEmailAt;
    }

    public function setVerifiedEmail(?\DateTimeInterface $verifiedEmail): self
    {
        $this->verifiedEmail = $verifiedEmailAt;

        return $this;
    }

    /**
     * @return Collection|Project[]
     */
    public function getProjects(): Collection
    {
        return $this->projects;
    }

    public function addProject(Project $project): self
    {
        if (!$this->projects->contains($project)) {
            $this->projects[] = $project;
            $project->setPerson($this);
        }

        return $this;
    }

    public function removeProject(Project $project): self
    {
        if ($this->projects->contains($project)) {
            $this->projects->removeElement($project);
            // set the owning side to null (unless already changed)
            if ($project->getPerson() === $this) {
                $project->setPerson(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Role[]
     */
    public function getPersonRoles(): Collection
    {
        return $this->personRoles;
    }

    public function addPersonRole(Role $personRole): self
    {
        if (!$this->personRoles->contains($personRole)) {
            $this->personRoles[] = $personRole;
            $personRole->addPerson($this);
        }

        return $this;
    }

    public function removePersonRole(Role $personRole): self
    {
        if ($this->personRoles->contains($personRole)) {
            $this->personRoles->removeElement($personRole);
            $personRole->removePerson($this);
        }

        return $this;
    }

    public function getFullname(){
        return $this->firstname." ".$this->lastname;
    }

    public function getGrpdConsentAt(): ?\DateTimeInterface
    {
        return $this->grpdConsentAt;
    }

    public function setGrpdConsentAt(\DateTimeInterface $grpdConsentAt): self
    {
        $this->grpdConsentAt = $grpdConsentAt;

        return $this;
    }

    public function getWebPage(): ?string
    {
        return $this->webPage;
    }

    public function setWebPage(?string $webPage): self
    {
        $this->webPage = $webPage;

        return $this;
    }

    public function getPhDYear(): ?string
    {
        return $this->phDYear;
    }

    public function setPhDYear(?string $phDYear): self
    {
        $this->phDYear = $phDYear;

        return $this;
    }

    public function getPhDUniversity(): ?string
    {
        return $this->phDUniversity;
    }

    public function setPhDUniversity(?string $phDUniversity): self
    {
        $this->phDUniversity = $phDUniversity;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getResetPasswordToken(): ?string
    {
        return $this->resetPasswordToken;
    }

    public function setResetPasswordToken(?string $resetPasswordToken): self
    {
        $this->resetPasswordToken = $resetPasswordToken;

        return $this;
    }

    public function getTokenExpireAt(): ?\DateTimeInterface
    {
        return $this->tokenExpireAt;
    }

    public function setTokenExpireAt(?\DateTimeInterface $tokenExpireAt): self
    {
        $this->tokenExpireAt = $tokenExpireAt;

        return $this;
    }

    /**
     * @return Collection|Nationality[]
     */
    public function getNationalities(): Collection
    {
        return $this->nationalities;
    }

    public function addNationality(Nationality $nationality): self
    {
        if (!$this->nationalities->contains($nationality)) {
            $this->nationalities[] = $nationality;
        }

        return $this;
    }

    public function removeNationality(Nationality $nationality): self
    {
        if ($this->nationalities->contains($nationality)) {
            $this->nationalities->removeElement($nationality);
        }

        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(?string $phoneNumber): self
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    public function getStatus(){
        $status = [];
        foreach ($this->getProjects as $project) {
           

        }

    }

    public function getPostalAddress(): ?PostalAddress
    {
        return $this->postalAddress;
    }

    public function setPostalAddress(PostalAddress $postalAddress): self
    {
        $this->postalAddress = $postalAddress;

        // set the owning side of the relation if necessary
        if ($this !== $postalAddress->getPerson()) {
            $postalAddress->setPerson($this);
        }

        return $this;
    }

    public function getFrenchLevel(): ?string
    {
        return $this->frenchLevel;
    }

    public function setFrenchLevel(?string $frenchLevel): self
    {
        $this->frenchLevel = $frenchLevel;

        return $this;
    }

    public function getEnglishLevel(): ?string
    {
        return $this->englishLevel;
    }

    public function setEnglishLevel(?string $englishLevel): self
    {
        $this->englishLevel = $englishLevel;

        return $this;
    }
    

    /**
     * @return Collection|Field[]
     */
    public function getFields(): Collection
    {
        return $this->fields;
    }

    public function addField(Field $field): self
    {
        if (!$this->fields->contains($field)) {
            $this->fields[] = $field;
            $field->addPerson($this);
        }

        return $this;
    }

    public function removeField(Field $field): self
    {
        if ($this->fields->contains($field)) {
            $this->fields->removeElement($field);
            $field->removePerson($this);
        }

        return $this;
    }


    /**
     * @return Collection|ScientificCouncil[]
     */
    public function getScientificCouncils(): Collection
    {
        return $this->scientificCouncils;
    }

    public function addScientificCouncil(ScientificCouncil $scientificCouncil): self
    {
        if (!$this->scientificCouncils->contains($scientificCouncil)) {
            $this->scientificCouncils[] = $scientificCouncil;
            $scientificCouncil->addPerson($this);
        }

        return $this;
    }

    public function removeScientificCouncil(ScientificCouncil $scientificCouncil): self
    {
        if ($this->scientificCouncils->contains($scientificCouncil)) {
            $this->scientificCouncils->removeElement($scientificCouncil);
            $scientificCouncil->removePerson($this);
        }

        return $this;
    }

    /**
     * QRO - Function that return uppercase from lastname. Mostly used for filenames
     * e.g : 
     *      - Robïché => ROBICHE
     *      - De patulacci => DE-PATULACCI
     * @return string|null
     */
    public function getLastnameSlug(): ?string 
    {
        $slugify = new Slugify();
        return strtoupper($slugify->slugify($this->lastname));
    }

    public function getGenderLabel(): ?string
    {
        switch ($this->gender) {
            case 1:
                return "Male";
            case 2:
                return "Female";
            case 3:
                return "Neutral";
            default:
                return "-";
        }
    }

    public function checkValidity() {
    	$errorList = array();
    	
//		if ($this->getTitle() == NULL) {$errorList[] = "Person title not set";}
		if ($this->getGender() == NULL) {$errorList[] = "Person gender not set";}
		if ($this->getLastname() == NULL) {$errorList[] = "Person last name not set";}
		if ($this->getFirstname() == NULL) {$errorList[] = "Person first name subject not setn";}
//		if ($this->getUsualLastname() == NULL) {$errorList[] = "Person usual last name not set";}
//		if ($this->getUsualFirstname() == NULL) {$errorList[] = "Person usual first name subject not set";}
		if ($this->getDateOfBirth() == NULL) {$errorList[] = "Person date of birth not set";}
		if ($this->getPlaceOfBirth() == NULL) {$errorList[] = "Person place of birth not set";}
		if ($this->getEmail() == NULL) {$errorList[] = "Email not set";}
//		if ($this->getWebPage() == NULL) {$errorList[] = "Personal web page not set";}
//		if ($this->getPhDYear() == NULL) {$errorList[] = "PHD year not set";}
//		if ($this->getPhDUniversity() == NULL) {$errorList[] = "PHD university not set";}
		if ($this->getNationalities() == NULL) {$errorList[] = "No nationality set";}
//		if ($this->getPhoneNumber() == NULL) {$errorList[] = "Phone number not set";}
		if ($this->getPostalAddress() == NULL) {$errorList[] = "Postal address not set";}
		if ($this->getFrenchLevel() == NULL) {$errorList[] = "French level set not set";}
		if ($this->getEnglishLevel() == NULL) {$errorList[] = "English level set not set";}
		
    	return $errorList;
	}


}
