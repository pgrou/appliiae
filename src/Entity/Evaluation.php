<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EvaluationRepository")
 */
class Evaluation
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ApplicationForm", inversedBy="evaluations")
     */
    private $applicationForm;

    /**
     * @ORM\Column(type="string", length=2, nullable=true)
     */
    private $grade;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Person", inversedBy="evaluations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $expert;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $comment;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $requestedAt;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isAccepted;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $remindedAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dueAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $thanksSentAt;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $internalNotes;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
	private $typeEvaluation;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getApplicationForm(): ?ApplicationForm
    {
        return $this->applicationForm;
    }

    public function setApplicationForm(?ApplicationForm $applicationForm): self
    {
        $this->applicationForm = $applicationForm;

        return $this;
    }

    public function getGrade(): ?string
    {
        return $this->grade;
    }

    public function setGrade(?string $grade): self
    {
        $this->grade = $grade;

        return $this;
    }

    public function getExpert(): ?Person
    {
        return $this->expert;
    }

    public function setExpert(?Person $expert): self
    {
        $this->expert = $expert;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getRequestedAt(): ?\DateTimeInterface
    {
        return $this->requestedAt;
    }

    public function setRequestedAt(?\DateTimeInterface $requestedAt): self
    {
        $this->requestedAt = $requestedAt;

        return $this;
    }

    public function getIsAccepted(): ?bool
    {
        return $this->isAccepted;
    }

    public function setIsAccepted(?bool $isAccepted): self
    {
        $this->isAccepted = $isAccepted;

        return $this;
    }

    public function getRemindedAt(): ?\DateTimeInterface
    {
        return $this->remindedAt;
    }

    public function setRemindedAt(?\DateTimeInterface $remindedAt): self
    {
        $this->remindedAt = $remindedAt;

        return $this;
    }

    public function getDueAt(): ?\DateTimeInterface
    {
        return $this->dueAt;
    }

    public function setDueAt(?\DateTimeInterface $dueAt): self
    {
        $this->dueAt = $dueAt;

        return $this;
    }

    public function getThanksSentAt(): ?\DateTimeInterface
    {
        return $this->thanksSentAt;
    }

    public function setThanksSentAt(?\DateTimeInterface $thanksSentAt): self
    {
        $this->thanksSentAt = $thanksSentAt;

        return $this;
    }

    public function getInternalNotes(): ?string
    {
        return $this->internalNotes;
    }

    public function setInternalNotes(?string $internalNotes): self
    {
        $this->internalNotes = $internalNotes;

        return $this;
    }

    public function getTypeEvaluation(): ?int
    {
        return $this->typeEvaluation;
    }

    public function getTypeEvaluationsString(): ?string
    {
    	switch ($this->typeEvaluation) {
    		case 1 : return "Expert report";
    		case 2 : return "Rapporteur report";
    		default : return "not defined";
    	}
    }

    public function setTypeEvaluation(?int $typeEvaluation): self
    {
        $this->typeEvaluation = $typeEvaluation;

        return $this;
    }
}
