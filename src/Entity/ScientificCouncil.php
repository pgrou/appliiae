<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ScientificCouncilRepository")
 */
class ScientificCouncil
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(max="255")
     */
    private $label;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Call", mappedBy="scientificCouncil")
     */
    private $calls;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Person", mappedBy="scientificCouncils")
     */
    private $members;

    public function __construct()
    {
        $this->calls = new ArrayCollection();
        $this->members = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(?\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @return Collection|Call[]
     */
    public function getCalls(): Collection
    {
        return $this->calls;
    }

    public function addCall(Call $call): self
    {
        if (!$this->calls->contains($call)) {
            $this->calls[] = $call;
            $call->setScientificCouncil($this);
        }

        return $this;
    }

    public function removeCall(Call $call): self
    {
        if ($this->calls->contains($call)) {
            $this->calls->removeElement($call);
            // set the owning side to null (unless already changed)
            if ($call->getScientificCouncil() === $this) {
                $call->setScientificCouncil(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Person[]
     */
    public function getMembers(): Collection
    {
        return $this->members;
    }

    public function addPerson(Person $person): self
    {
        if (!$this->members->contains($person)) {
            $this->members[] = $person;
            $person->addScientificCouncil($this);
        }

        return $this;
    }

    public function removePerson(Person $person): self
    {
        if ($this->members->contains($person)) {
            $this->members->removeElement($person);
            $person->removeScientificCouncil($this);
        }

        return $this;
    }
}
