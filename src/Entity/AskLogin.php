<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface ;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AskLoginRepository")
 * @UniqueEntity(
 * fields={"email"},
 * message="This email already exists."
 * )
 */
class AskLogin implements UserInterface
{
    public static $status_demand   = 1;
    public static $status_accepted = 2;
    public static $status_refused  = 3;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotNull(message="Lastname field cannot be empty")
     */
    private $lastname;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Email()
     * @Assert\NotBlank
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(min="8", minMessage="At least 8 char")
     */
    private $password;

    /**
     * @Assert\EqualTo(propertyPath="password", message="Passwords must be the same")
     */
    private $confirmPassword;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @ORM\Column(type="string", length=1024)
     */
    private $project;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $grpdConsentAt;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $status;
    
    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $processedAt;


    public function __construct()
    {
        
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(?string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getConfirmPassword()
    {
        return $this->confirmPassword;
    }
    
    public function setConfirmPassword(string $confirmPassword): self
    {
        $this->confirmPassword = $confirmPassword;
        return $this;
    }
    
    public function getUsername(){
        return $this->email;
    }

    public function eraseCredentials(){}
        
    public function getSalt(){}
    
    public function getRoles(){
        return array('ROLE_USER');
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getProcessedAt(): ?\DateTimeInterface
    {
        return $this->processedAt;
    }

    public function setProcessedAt(?\DateTimeInterface $processedAt): self
    {
        $this->processedAt = $processedAt;

        return $this;
    }

    public function getProject()
    {
        return $this->project;
    }

    public function setProject(string $project): self
    {
        $this->project = $project;

        return $this;
    }

    public function getFullname(){
        return $this->firstname." ".$this->lastname;
    }

    public function getGrpdConsentAt(): ?\DateTimeInterface
    {
        return $this->grpdConsentAt;
    }

    public function setGrpdConsentAt(\DateTimeInterface $grpdConsentAt): self
    {
        $this->grpdConsentAt = $grpdConsentAt;

        return $this;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus($status): self
    {
        $this->status = $status;

        return $this;
    }
}
