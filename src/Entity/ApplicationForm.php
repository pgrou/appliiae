<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints\Valid;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ApplicationFormRepository")
 */
class ApplicationForm
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(max="255")
     */
    private $currentEmployer;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(max="255")
     */
    private $positionTitle;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * 
     */
    private $positionSince;

    /**
     * @ORM\Column(type="text", nullable=true)
     * 
     */
    private $previousPositions;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $iasKnowledge;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $previousIasMember;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $majorPuplications;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $motivations;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $ongoingApplication;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $benefits;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $biography;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(max="255")
     */
    private $sabbatical;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $financialSupport;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(max="255")
     */
    private $usualSalary;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $accompanying;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $resideInNantes;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $lastUpdateAt;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Evaluation", mappedBy="applicationForm")
     */
    private $evaluations;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Call", inversedBy="applicationForms")
     * @ORM\JoinColumn(nullable=false)
     */
    private $call;

    /**
     * @Assert\Valid
     * @ORM\ManyToOne(targetEntity="App\Entity\Project", inversedBy="applicationForms")
     * @ORM\JoinColumn(nullable=false)
     */
    private $project;

    /**
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $submittedAt;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(max="255")
     */
    private $finalDecision;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $Choice1NumberMonths;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $Choice1StartMonth;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $Choice2NumberMonths;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $Choice2StartMonth;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $fileCurriculumVitae;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $fileApplicationForm;

   
    public function __construct()
    {
        $this->evaluations = new ArrayCollection();
        $this->requestedPeriods = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCurrentEmployer(): ?string
    {
        return $this->currentEmployer;
    }

    public function setCurrentEmployer(?string $currentEmployer): self
    {
        $this->currentEmployer = $currentEmployer;

        return $this;
    }

    public function getPositionTitle(): ?string
    {
        return $this->positionTitle;
    }

    public function setPositionTitle(?string $positionTitle): self
    {
        $this->positionTitle = $positionTitle;

        return $this;
    }

    public function getPositionSince(): ?\DateTimeInterface
    {
        return $this->positionSince;
    }

    public function setPositionSince(?\DateTimeInterface $positionSince): self
    {
        $this->positionSince = $positionSince;

        return $this;
    }

    public function getPreviousPositions(): ?string
    {
        return $this->previousPositions;
    }

    public function setPreviousPositions(?string $previousPositions): self
    {
        $this->previousPositions = $previousPositions;

        return $this;
    }

    public function getIasKnowledge(): ?string
    {
        return $this->iasKnowledge;
    }

    public function setIasKnowledge(?string $iasKnowledge): self
    {
        $this->iasKnowledge = $iasKnowledge;

        return $this;
    }

    public function getPreviousIasMember(): ?string
    {
        return $this->previousIasMember;
    }

    public function setPreviousIasMember(?string $previousIasMember): self
    {
        $this->previousIasMember = $previousIasMember;

        return $this;
    }

    public function getMajorPuplications(): ?string
    {
        return $this->majorPuplications;
    }

    public function setMajorPuplications(?string $majorPuplications): self
    {
        $this->majorPuplications = $majorPuplications;

        return $this;
    }

    public function getMotivations(): ?string
    {
        return $this->motivations;
    }

    public function setMotivations(?string $motivations): self
    {
        $this->motivations = $motivations;

        return $this;
    }

    public function getOngoingApplication(): ?string
    {
        return $this->ongoingApplication;
    }

    public function setOngoingApplication(?string $ongoingApplication): self
    {
        $this->ongoingApplication = $ongoingApplication;

        return $this;
    }

    public function getBenefits(): ?string
    {
        return $this->benefits;
    }

    public function setBenefits(?string $benefits): self
    {
        $this->benefits = $benefits;

        return $this;
    }

    public function getBiography(): ?string
    {
        return $this->biography;
    }

    public function setBiography(?string $biography): self
    {
        $this->biography = $biography;

        return $this;
    }

    public function getSabbatical(): ?string
    {
        return $this->sabbatical;
    }

    public function setSabbatical(?string $sabbatical): self
    {
        $this->sabbatical = $sabbatical;

        return $this;
    }

    public function getFinancialSupport(): ?string
    {
        return $this->financialSupport;
    }

    public function setFinancialSupport(?string $financialSupport): self
    {
        $this->financialSupport = $financialSupport;

        return $this;
    }

    public function getUsualSalary(): ?string
    {
        return $this->usualSalary;
    }

    public function setUsualSalary(?string $usualSalary): self
    {
        $this->usualSalary = $usualSalary;

        return $this;
    }

    public function getAccompanying(): ?string
    {
        return $this->accompanying;
    }

    public function setAccompanying(?string $accompanying): self
    {
        $this->accompanying = $accompanying;

        return $this;
    }

    public function getResideInNantes(): ?bool
    {
        return $this->resideInNantes;
    }

    public function setResideInNantes(?bool $resideInNantes): self
    {
        $this->resideInNantes = $resideInNantes;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getSubmittedAt(): ?\DateTimeInterface
    {
        return $this->submittedAt;
    }

    public function setSubmittedAt(\DateTimeInterface $submittedAt): self
    {
        $this->submittedAt = $submittedAt;

        return $this;
    }

    public function getLastUpdateAt(): ?\DateTimeInterface
    {
        return $this->lastUpdateAt;
    }

    public function setLastUpdateAt(\DateTimeInterface $lastUpdateAt): self
    {
        $this->lastUpdateAt = $lastUpdateAt;

        return $this;
    }

    /**
     * @return Collection|Evaluation[]
     */
    public function getEvaluations(): Collection
    {
        return $this->evaluations;
    }

    public function addEvaluation(Evaluation $evaluation): self
    {
        if (!$this->evaluations->contains($evaluation)) {
            $this->evaluations[] = $evaluation;
            $evaluation->setApplicationForm($this);
        }

        return $this;
    }

    public function removeEvaluation(Evaluation $evaluation): self
    {
        if ($this->evaluations->contains($evaluation)) {
            $this->evaluations->removeElement($evaluation);
            // set the owning side to null (unless already changed)
            if ($evaluation->getApplicationForm() === $this) {
                $evaluation->setApplicationForm(null);
            }
        }

        return $this;
    }

    public function getCall(): ?Call
    {
        return $this->call;
    }

    public function setCall(?Call $call): self
    {
        $this->call = $call;

        return $this;
    }

    public function getProject(): ?Project
    {
        return $this->project;
    }

    public function setProject(?Project $project): self
    {
        $this->project = $project;

        return $this;
    }

    public function getFinalDecision(): ?string
    {
        return $this->finalDecision;
    }

    public function setFinalDecision(?string $finalDecision): self
    {
        $this->finalDecision = $finalDecision;

        return $this;
    }

    public function getChoice1NumberMonths(): ?int
    {
        return $this->Choice1NumberMonths;
    }

    public function setChoice1NumberMonths(?int $Choice1NumberMonths): self
    {
        $this->Choice1NumberMonths = $Choice1NumberMonths;

        return $this;
    }

    public function getChoice1StartMonth(): ?int
    {
        return $this->Choice1StartMonth;
    }

    public function setChoice1StartMonth(?int $Choice1StartMonth): self
    {
        $this->Choice1StartMonth = $Choice1StartMonth;

        return $this;
    }

    public function getChoice2NumberMonths(): ?int
    {
        return $this->Choice2NumberMonths;
    }

    public function setChoice2NumberMonths(?int $Choice2NumberMonths): self
    {
        $this->Choice2NumberMonths = $Choice2NumberMonths;

        return $this;
    }

    public function getChoice2StartMonth(): ?int
    {
        return $this->Choice2StartMonth;
    }

    public function setChoice2StartMonth(?int $Choice2StartMonth): self
    {
        $this->Choice2StartMonth = $Choice2StartMonth;

        return $this;
    }

    public function getFileCurriculumVitae(): ?string
    {
        return $this->fileCurriculumVitae;
    }

    public function setFileCurriculumVitae(?string $fileCurriculumVitae): self
    {
        $this->fileCurriculumVitae = $fileCurriculumVitae;

        return $this;
    }

    public function getPerson(): Person
    {
        //TODO : Verify if that expression return something. It normally should but if not, program would bug. 
        return $this->getProject()->getPerson();
    }
    public function getAllFiles(){
        $files=[];
        $project = $this->getProject();
        $cv ='/uploads/ApplicationForm/'.$this->getId().'/'.$this->getFileCurriculumVitae();
        $applicationPdf = '/uploads/ApplicationForm/'.$this->getId().'/'.$this->getFileApplicationForm();
        $project = '/uploads/Project/'.$this->getProject()->getId().'/'.$this->getProject()->getFileProject();
        $publication1 = '/uploads/Project/'.$this->getProject()->getId().'/'.$this->getProject()->getFilePublication1();
        $publication2 = '/uploads/Project/'.$this->getProject()->getId().'/'.$this->getProject()->getFilePublication2();
        $files[] = $cv;
        $files[] = $applicationPdf;
        $files[] = $project;
        $files[] = $publication1;
        $files[] = $publication2;
        return $files;
    }
    public function getIsArchive(){
        if($this->call->getScientificCouncil()->getDate()>new \Datetime()){
            return false;
        }else{
            return true;
        }
    }

    public function getPublicStatus(){
        if($this->getIsArchive()){
            return 'Archive';
        }elseif($this->submittedAt !== null){
            return 'Submitted';
        }else{
            return 'initiated';
        }
    }
    public function getPrivateStatus(){
        if($this->getIsArchive()){
            return 'Archive';
        }elseif($this->submittedAt !== null){
            return '1-Submitted';
        }else{
            return '0-Initiated';
        }
    }

    public function getFileApplicationForm(): ?string
    {
        return $this->fileApplicationForm;
    }

    public function setFileApplicationForm(?string $fileApplicationForm): self
    {
        $this->fileApplicationForm = $fileApplicationForm;

        return $this;
    }

    public function checkValidity() {
        $errorList = array();

        if ($this->getProject() == NULL) {
            $errorList[] = "No project";
        } else {
            $errorList = array_merge($errorList, $this->getProject()->checkValidity());
        }
        
        if ($this->getPerson() == NULL) {
            $errorList[] = "No person";
        } else {
            $errorList = array_merge($errorList, $this->getPerson()->checkValidity());
        }
        
//      if ($this->getIasKnowledge() == NULL) {$errorList[] = "IAS awareness not set";}
//      if ($this->getOngoingApplication() == NULL) {$errorList[] = "No ongoing application";}
        if ($this->getMajorPuplications() == NULL) {$errorList[] = "No main publication";}
        if ($this->getMotivations() == NULL) {$errorList[] = "No motivation defined";}
        if ($this->getBenefits() == NULL) {$errorList[] = "No benefit defined";}
        if ($this->getBiography() == NULL) {$errorList[] = "No biography defined";}
        
        if ($this->getCurrentEmployer() == NULL) {$errorList[] = "Current employer not set";}
        if ($this->getPositionTitle() == NULL) {$errorList[] = "No title defined";}
        if ($this->getPositionSince() == NULL) {$errorList[] = "No start position defined";}
        if ($this->getPreviousPositions() == NULL) {$errorList[] = "No previous position defined";}
        if ($this->getChoice1StartMonth() == NULL) {$errorList[] = "Choice 1: no start month";}
        if ($this->getChoice1NumberMonths() == NULL) {$errorList[] = "Choice 1: no duration";}
        if ($this->getChoice2NumberMonths() == NULL) {$errorList[] = "Choice 2: no start month";}
        if ($this->getChoice2StartMonth() == NULL) {$errorList[] = "Choice 2: no duration";}
        if ($this->getFinancialSupport() == NULL) {$errorList[] = "No financial support defined";}
        if ($this->getUsualSalary() == NULL) {$errorList[] = "No salary set";}
        if ($this->getAccompanying() == NULL) {$errorList[] = "Accompanying not set";}
        if ($this->getResideInNantes() == NULL) {$errorList[] = "You HAVE TO reside in Nantes";}

        if ($this->getFileCurriculumVitae() == NULL) {$errorList[] = "CV not uploaded";}
//      if ($this->getFileApplicationForm() == NULL) {$errorList[] = "No application file";}
        if ($this->getProject() != NULL) {
            if ($this->getProject()->getFileProject() == NULL) {$errorList[] = "No project file uploaded";}
            if ($this->getProject()->getFilePublication1() == NULL) {$errorList[] = "First publication missing";}
            if ($this->getProject()->getFilePublication2() == NULL) {$errorList[] = "Second publication missing";}
        }

/*
        if ($this->getCall() == NULL) {$errorList[] = "Error";}
        if ($this->getEvaluations() == NULL) {$errorList[] = "Error";}
        if ($this->getIsArchive() == NULL) {$errorList[] = "Error";}
        if ($this->getPrivateStatus() == NULL) {$errorList[] = "Error";}
        if ($this->getPublicStatus() == NULL) {$errorList[] = "Error";}
*/
//      if ($this->getSabbatical() == NULL) {$errorList[] = "Error";}
                
        return $errorList;
    }

}
