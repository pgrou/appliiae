<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LanguageRepository")
 */
class Language
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=3, nullable=true)
     */
    private $iso639_3;

    /**
     * @ORM\Column(type="string", length=3, nullable=true)
     */
    private $iso639_2;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $label_en;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $label_fr;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIso6393(): ?string
    {
        return $this->iso639_3;
    }

    public function setIso6393(?string $iso639_3): self
    {
        $this->iso639_3 = $iso639_3;

        return $this;
    }

    public function getIso6392(): ?string
    {
        return $this->iso639_2;
    }

    public function setIso6392(?string $iso639_2): self
    {
        $this->iso639_2 = $iso639_2;

        return $this;
    }

    public function getLabelEn(): ?string
    {
        return $this->label_en;
    }

    public function setLabelEn(string $label_en): self
    {
        $this->label_en = $label_en;

        return $this;
    }

    public function getLabelFr(): ?string
    {
        return $this->label_fr;
    }

    public function setLabelFr(?string $label_fr): self
    {
        $this->label_fr = $label_fr;

        return $this;
    }
}
