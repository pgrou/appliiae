<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProjectRepository")
 */
class Project
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     * @Assert\Length(
     *      max = 255,
     *      maxMessage = "Title cannot be longer than 255 characters"
     * )
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank
     */
    private $summary;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $mainSubjects;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $previousPublicationsEnlightning;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Field", inversedBy="projects")
     * @Assert\NotBlank
     */
    private $mainField;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(
     *      max = 255,
     *      maxMessage = "Specialty cannot be longer than 255 characters"
     * )
     */
    private $specialty;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Person", inversedBy="projects")
     * @ORM\JoinColumn(nullable=false)
     */
    private $person;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ApplicationForm", mappedBy="project")
     */
    private $applicationForms;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $conciliatePreviousWorks;
     /**  
     * @ORM\Column(type="string", length=255, nullable=true)
     * 
     */
    private $fileProject;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * 
     */
    private $filePublication1;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * 
     */
    private $filePublication2;



    public function __construct()
    {
        $this->applicationForms = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getSummary(): ?string
    {
        return $this->summary;
    }

    public function setSummary(string $summary): self
    {
        $this->summary = $summary;

        return $this;
    }

    public function getMainSubjects(): ?string
    {
        return $this->mainSubjects;
    }

    public function setMainSubjects(?string $mainSubjects): self
    {
        $this->mainSubjects = $mainSubjects;

        return $this;
    }

    public function getPreviousPublicationsEnlightning(): ?string
    {
        return $this->previousPublicationsEnlightning;
    }

    public function setPreviousPublicationsEnlightning(?string $previousPublicationsEnlightning): self
    {
        $this->previousPublicationsEnlightning = $previousPublicationsEnlightning;

        return $this;
    }

    public function getMainField(): ?Field
    {
        return $this->mainField;
    }

    public function setMainField(?Field $mainField): self
    {
        $this->mainField = $mainField;

        return $this;
    }

    public function getSpecialty(): ?string
    {
        return $this->specialty;
    }

    public function setSpecialty(?string $specialty): self
    {
        $this->specialty = $specialty;

        return $this;
    }

    public function getPerson(): ?Person
    {
        return $this->person;
    }

    public function setPerson(?Person $person): self
    {
        $this->person = $person;

        return $this;
    }

    /**
     * @return Collection|ApplicationForm[]
     */
    public function getApplicationForms(): Collection
    {
        return $this->applicationForms;
    }

    public function addApplicationForm(ApplicationForm $applicationForm): self
    {
        if (!$this->applicationForms->contains($applicationForm)) {
            $this->applicationForms[] = $applicationForm;
            $applicationForm->setProject($this);
        }

        return $this;
    }

    public function removeApplicationForm(ApplicationForm $applicationForm): self
    {
        if ($this->applicationForms->contains($applicationForm)) {
            $this->applicationForms->removeElement($applicationForm);
            // set the owning side to null (unless already changed)
            if ($applicationForm->getProject() === $this) {
                $applicationForm->setProject(null);
            }
        }

        return $this;
    }

    public function getConciliatePreviousWorks(): ?string
    {
        return $this->conciliatePreviousWorks;
    }

    public function setConciliatePreviousWorks(?string $conciliatePreviousWorks): self
    {
        $this->conciliatePreviousWorks = $conciliatePreviousWorks;

        return $this;
    }


    public function getFileProject(): ?string
    {
        return $this->fileProject;
    }

    public function setFileProject(?string $fileProject): self
    {
        $this->fileProject = $fileProject;

        return $this;
    }

    public function getFilePublication1(): ?string
    {
        return $this->filePublication1;
    }

    public function setFilePublication1(?string $filePublication1): self
    {
        $this->filePublication1 = $filePublication1;

        return $this;
    }

    public function getFilePublication2(): ?string
    {
        return $this->filePublication2;
    }

    public function setFilePublication2(?string $filePublication2): self
    {
        $this->filePublication2 = $filePublication2;

        return $this;
    }

    public function checkValidity() {
    	$errorList = array();
    	
		if ($this->getTitle() == NULL) {$errorList[] = "Project title not set";}
		if ($this->getSummary() == NULL) {$errorList[] = "Project summary not set";}
		if ($this->getMainSubjects() == NULL) {$errorList[] = "Project main subject not defined";}
		if ($this->getPreviousPublicationsEnlightning() == NULL) {$errorList[] = "Project main publication not defined";}
		if ($this->getMainField() == NULL) {$errorList[] = "Project main field not set";}
		if ($this->getSpecialty() == NULL) {$errorList[] = "Project speciality not set";}
    	
    	return $errorList;
	}

}
